%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

classdef ElastoplasticSliding<Elastoplastic
    %elastoplastic problem which implements the event-driven solution
    %ASSUMPTION: one piston, stretching
    properties
        findVelocityMethod;
    end
    methods
        %inherit the constructor
        function obj=ElastoplasticSliding(D,L,a,sm,sp,findVelocityMethod)
            obj@Elastoplastic(D,L,a,sm,sp);
            obj.findVelocityMethod=findVelocityMethod;
        end
        
        function x1=getIntersectionPoint(obj,x0,v,I,eps)
            if (numel(I)==0)|| (norm(v,1)<eps)
                x1=x0;
            else
                A=diag(obj.a);
                min=realmax;
                minSpr=I(1);
                sp=A*obj.ep;
                sm=A*obj.em;
                
                for i=I
                    %normal to the boundary in V
                    %each column of vP is the respective n_i
                    %since it is produced by projecting e_i
                    n=obj.vBasis*obj.vP(:,i);
                    if v.'*A*n>=eps
                        t=getDT(A,x0,v,n,sp(i));
                    elseif -v.'*A*n>=eps
                        t=getDT(A,x0,v,n,sm(i));
                    else
                        t=realmax;
                    end
                    
                    if t<0
                        error(strcat('negative t=',num2str(t),...
                            ' for i=',num2str(i)));
                    end
                    
                    if t<min
                        min=t;
                        minSpr=i;
                    end
                end
                if(min==realmax)
                    x1=x0;
                else
                    x1=x0+v*min;
                end
            end
        end
        
        %will not work after R2016a
        function [v,I]=getSlidingVelocityByQuadprog(obj,x0,eps)
            constr=[];
            I=[];
            
            for i=1:obj.m
                if (abs(x0(i)-obj.em(i))<eps)
                    c=zeros(1,obj.m);
                    c(i)=-1;
                    constr=[constr; c];
                elseif (abs(x0(i)-obj.ep(i))<eps)
                    c=zeros(1,obj.m);
                    c(i)=1;
                    constr=[constr; c];
                else
                    I=[I i];
                end
            end
            b=zeros(size(constr,1),1);
            beq=zeros(size(obj.vOrt,1),1);
            v0=obj.pistonsH;
            opts = optimoptions('quadprog','Algorithm',obj.findVelocityMethod,'Display','off');
            [v,val,exitflag]=quadprog(diag(obj.a),-diag(obj.a)*v0,...
                constr,b,obj.vOrt,beq,[],[],[],opts);
            if exitflag~=1
                error(strcat('cannot optimize, fval: ',num2str(val),...
                    ', exitflag: ',num2str(exitflag)))
            end
            
            if norm(v)<eps
                v=zeros(size(v));
            end
        end
        
        function [v,I]=getSlidingVelocityBySQP(obj,x0,eps)
            A=diag(obj.a);
            constr=[];
            I=[];
            
            %find active and inactive(I) constraints
            for i=1:obj.m
                if (abs(x0(i)-obj.em(i))<eps)
                    c=zeros(1,obj.m);
                    c(i)=-1;
                    constr=[constr; c];
                elseif (abs(x0(i)-obj.ep(i))<eps)
                    c=zeros(1,obj.m);
                    c(i)=1;
                    constr=[constr; c];
                else
                    I=[I i];
                end
            end
            b=zeros(size(constr,1),1);
            beq=zeros(size(obj.vOrt,1),1);
            v0=obj.pistonsH;
            
            
            %%use fmincon with sqp method - maybe not the best but working
            %%approach (mathematically I have a projection on a cone)
            function [val,grad]=fun(x)
                %disp(x); %debug output
                %disp(A);
                val=x.'*A*x/2-v0.'*A*x;
                %disp(val);
                %disp('xxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
                grad=A*(x-v0);
            end
            opts = optimoptions('fmincon','SpecifyObjectiveGradient',true,...
                'Algorithm','sqp','ConstraintTolerance',eps,...
                'FunctionTolerance',eps,'OptimalityTolerance',eps,...
                'StepTolerance',eps, 'Display','off');
            [v,fval,exitflag,output]=fmincon(@fun,v0,constr,b,obj.vOrt,beq,[],[],[],opts);
            
            
            
            %%%I need the active-set method, but it was removed from
            %%%quadprog
            
            %opts = optimoptions('quadprog','Algorithm','active-set',...
            %    'Display','off',... %'iter-detailed',
            %'OptimalityTolerance',eps,'ConstraintTolerance',eps,...
            %    'StepTolerance',eps,'MaxIterations',1000);
            %[v,val,exitflag,output,lambda]=quadprog(diag(obj.a),-diag(obj.a)*v0,...
            %    constr,b,obj.vOrt,beq,[],[],[],opts);
            if (exitflag~=1) && (exitflag~=2)
                error(strcat('cannot optimize, fval: ',num2str(fval),...
                    ', exitflag: ',num2str(exitflag)))
            end
            if norm(v)<eps
                v=zeros(size(v));
            end
        end
        
        %{
        %not working properly, do not use!!!
        function [v,I]=getSlidingVelocityOptV(obj,x0,eps)
            constr=[];
            I=[];
            
            for i=1:obj.m
                if (abs(x0(i)-obj.em(i))<eps)
                    c=-obj.vP(:,i).';
                    constr=[constr; c];
                elseif (abs(x0(i)-obj.ep(i))<eps)
                    c=obj.vP(:,i).';
                    constr=[constr; c];
                else
                    I=[I i];
                end
            end
            b=zeros(size(constr,1),1);
            %gonna solve for velocity in V
            v0=obj.pistonsV;
            vA=obj.vBasis.'*diag(obj.a)*obj.vBasis;
            
            
            opts = optimoptions('quadprog','Algorithm','active-set','Display','off');
            [vV,val,exitflag]=quadprog(vA,-vA*v0,...
                constr,b,[],[],[],[],[],opts);
            v=obj.vBasis*vV;
            
            if exitflag~=1
                error(strcat('cannot optimize, fval: ',num2str(val),...
                    ', exitflag: ',num2str(exitflag)))
            end
        end
        %}
        
        function [X,V,I]=getSolutionStages(obj,nstages)
            %points of trajectory
            X=zeros(obj.m,nstages);
            %velocities
            V=zeros(obj.m,nstages);
            V(:,1)=obj.pistonsH;
            %inactive constraints
            I=cell(1,nstages);
            I{1}=1:obj.m;
            
            %+1 because i represents hit points, not stages
            for i=2:nstages+1
                %find first intersection point
                X(:,i)=obj.getIntersectionPoint(X(:,i-1),V(:,i-1),I{i-1},1e-9);
                %find sliding velocity
                [V(:,i),I{i}]=obj.getSlidingVelocityByQuadprog(X(:,i),1e-9);
            end
        end
        
    end
end


%returns time until intrsection with a plane
function dt=getDT(A, x0, v, n, c)
dt=(c-n.'*A*x0)/(n.'*A*v);
end