classdef SpringsElementVisual<handle
    %SPRINGSELEMENTVISUAL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        visualAxes;
        defaultNodesX;
        springs;
        D;
        
        %visual elements
        line1=[];
        line2=[];
        line3=[];
    end
    
    methods
        function obj=SpringsElementVisual(visualAxes,...
            defaultNodesX,springs,D,e,p,x,nodesCoords)
            obj.visualAxes=visualAxes;
            obj.defaultNodesX=defaultNodesX;
            obj.springs=springs;
            obj.D=D;
            
            [leftx,elastplastx,rightx,y]=obj.getCoords(e,p,x,nodesCoords);
            hold on            
            for i=1:length(obj.springs)
                [xdata1,ydata1,xdata2,ydata2,xdata3,ydata3]=obj.getDrawingData(i,e(i),p(i),x(i),leftx(i),elastplastx(i),rightx(i),y(i));
                obj.line1=[obj.line1 line(xdata1,ydata1,'Color','black','LineWidth',1)];
                obj.line2=[obj.line2 line(xdata2,ydata2,'Color','black','LineWidth',1)];
                obj.line3=[obj.line3 line(xdata3,ydata3,'Color','black','LineWidth',1)];                
            end                       
            hold off        
        end
        
        function update(obj,e,p,x,nodesCoords)
            [leftx,elastplastx,rightx,y]=obj.getCoords(e,p,x,nodesCoords);
            for i=1:length(obj.springs)
                 [xdata1,ydata1,xdata2,ydata2,xdata3,ydata3]=obj.getDrawingData(i,e(i),p(i),x(i),leftx(i),elastplastx(i),rightx(i),y(i));
                 obj.line1(i).XData=xdata1;
                 obj.line1(i).YData=ydata1;
                 obj.line2(i).XData=xdata2;
                 obj.line2(i).YData=ydata2;
                 obj.line3(i).XData=xdata3;                 
                 obj.line3(i).YData=ydata3;
            end            
            
        end
        
        function [xdata1,ydata1,xdata2,ydata2,xdata3,ydata3]=getDrawingData(obj,i,e,p,x,leftx,elastplastx,rightx,y)
            xdata1=[leftx leftx+obj.springs(i).l1];
            ydata1=[y y];
                
            wavelen=(elastplastx-leftx-obj.springs(i).l1-obj.springs(i).l2)/obj.springs(i).ek;                
            for j=1:obj.springs(i).ek
                xdata1=[xdata1 xdata1(end)+wavelen/4        xdata1(end)+wavelen*3/4      xdata1(end)+wavelen];
                ydata1=[ydata1 ydata1(end)+obj.springs(i).h ydata1(end)-obj.springs(i).h ydata1(end)];
            end
            xdata1=[xdata1 elastplastx+obj.springs(i).l3];
            ydata1=[ydata1 y];
            %plastic part
            xdata1=[xdata1 xdata1(end)                  rightx-obj.springs(i).l4      rightx-obj.springs(i).l4];
            ydata1=[ydata1 ydata1(end)+obj.springs(i).h ydata1(end)+obj.springs(i).h  ydata1(end)];
        
            %arrowhead
            xdata2=[xdata1(end)-obj.springs(i).aw xdata1(end) xdata1(end)+obj.springs(i).aw];
            ydata2=[ydata1(end)+obj.springs(i).aw ydata1(end) ydata1(end)+obj.springs(i).aw];
            
            %l5
            xdata3=[rightx-obj.springs(i).l4-obj.springs(i).l5 rightx];
            ydata3=[y                                          y];
            
        end
                
        function [leftx,elastplastx,rightx,y]=getCoords(obj,e,p,x,nodesCoords)
           [leftx,elastplastx,rightx,y]=deal(zeros(length(obj.springs),1));
           for i=1:length(obj.springs)
               leftx(i)=nodesCoords(obj.springs(i).lnode);
               elastplastx(i)=leftx(i)+e(i)+obj.springs(i).defElastLen;
               rightx(i)=nodesCoords(obj.springs(i).rnode);
               y(i)=obj.springs(i).y;
           end
        end
        
    end
    
end

