%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

function f = piecewiseLinPer(t,T,A)
%piecewise-linear periodic function with a period like:
% /\
%   \/
%vectroized on t
%T - period
%A - amplitude

%time w.r. to current period
t1=mod(t+0.25.*T,T);
increasing=t1<T/2;
theta=zeros(size(t));
theta(increasing)=2/T.*t1(increasing);
theta(~increasing)=(T-t1(~increasing)).*2/T;
f=2*A.*theta-A;
end

