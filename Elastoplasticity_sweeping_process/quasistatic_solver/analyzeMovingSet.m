%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}


function [Ain,r,nv]=analyzeMovingSet(problem,l,hCoords)
%returns
%Ain - non-redundand inequality constraints, 
%r   - minimal possible rank of dim V constraints: if 0 then there may be 
%      parallel facets in the moving set (?)
%nv  - #of vertices in the moving set


dimV=size(problem.vBasis,2);
offset=problem.getOffset(l,hCoords);

A=[problem.vBasis; -problem.vBasis];
b=[problem.ep+offset;-problem.em-offset];

%[Anr,bnr]=noredund(A, b);



vert=lcon2vert(A,b,[],[]);
nv=size(vert,1);
if nv>0
    
    [Ain,bin,Aeq,beq]=vert2lcon(vert);
    
    
    r=intmax;
    for i=1:size(Ain,1)
        r=min(r,reqRank(Ain,[],dimV,i));
    end
    
    %disp(['noredund: ',num2str(size(Anr,1))]);
    
    %disp('lcon2vert2lcon:')
    
    %disp(['# vert: ',num2str(nv)]);
    %disp(['# inequalities: ',num2str(size(Ain,1))]);
    %disp(['min rank: ',num2str(r)]);
    %disp(['# equalities: ',num2str(size(Aeq,1))]);
else
    Ain=NaN;
    r=NaN;
end
end
function [r,M1]= reqRank(Ain,M,depths,currInd)
M=[M;Ain(currInd,:)];
depths=depths-1;
if depths>0
    r=intmax;
    for i=currInd+1:size(Ain,1)
        r=min(r,reqRank(Ain,M,depths,i));
    end
    M1=NaN;
else
    r=rank(M,1e-9);
    M1=M;
    %disp(['rank M:', num2str(r)]);
end
end