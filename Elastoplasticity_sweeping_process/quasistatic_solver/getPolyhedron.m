%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

function [vert, faces] = getPolyhedron(vBasis,em,ep)
%GETPOLYHEDRON  
%vert - vertices
%faces - indecies of faces
n=size(em,1);
boxConstr=[eye(n);-eye(n)];
b=[ep;-em];
vert=lcon2vert(boxConstr*vBasis,b);
[facesCells,maxFaceSize]=detriangulate(vert, convhull(vert(:,1),vert(:,2),vert(:,3)));

nFaces=size(facesCells,2);

faces=zeros(nFaces,maxFaceSize);
for j=1:nFaces
    faces(j,1:size(facesCells{j},2))=facesCells{j};
    for k=(size(facesCells{j},2)+1):maxFaceSize
        faces(j,k)=NaN;
    end
end
end
