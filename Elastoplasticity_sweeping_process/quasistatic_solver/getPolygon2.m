%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

function vert= getPolygon2(vBasis,em,ep)
%GETPOLYGON  
%vert - vertices
%faces - indecies of faces
n=size(em,1);
boxConstr=[eye(n);-eye(n)];
b=[ep;-em];
vert1=lcon2vert(boxConstr*vBasis,b);
ind=sortVertices(vert1.',[1 0;0 1]);
k=size(ind,2);
vert=zeros(k,2);
for i=1:k
    vert(i,:)=vert1(ind(i),:);
end
end
