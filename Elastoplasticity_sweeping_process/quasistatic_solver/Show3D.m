%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

classdef Show3D<VisualRepresInterface
    %shows the moving set as a section of a cuboid of elastic boundaries
    
    properties
        %visual elements
        solutionMarker,movingSet,box;
        %box data
        boxVert,boxFaces;
        
        problem,hCoords,l;
        T;
        %solutions data:
        %1st dim - components
        %2nd dim - different times
        %3rd dim - different solutions        
        %agreement: size(Y,1)=problem.m, size(Y,2)==length(T)
        Y;        
    end
    
    methods
        function obj=Show3D(problem, hCoords, l,T,Y)
            obj.T=T;
            obj.Y=Y;
            obj.problem=problem;
            obj.hCoords=hCoords;
            obj.l=l;
            
        end
        
        function initialDrawing(obj,visualAxes,tn)   
            axes(visualAxes);
            %drawing axis arrows
            hold on
            quiver3([0,0,0],[0,0,0],[0,0,0],[1,0,0],[0,1,0],[0,0,1],'-k','AutoScale','off');
            
            %initial drawing of the moving set
            offset=obj.problem.getOffset(obj.l(obj.T(tn)), obj.hCoords(obj.T(tn)));
            C=getPolygon(obj.problem.vBasis,obj.problem.vOrt, obj.problem.em+offset,obj.problem.ep+offset);

            obj.movingSet=patch(C(1,:),C(2,:),C(3,:),'blue');

            
            %initial drawing of solution
            
            obj.solutionMarker=plot3(squeeze(obj.Y(1,tn,:)),squeeze(obj.Y(2,tn,:)),...
                squeeze(obj.Y(3,tn,:)),...
                'o','MarkerFaceColor','red','Color','red');
            
            
            %setup and initial drawing of the box
            obj.setupBox;
            
            obj.box=patch('Faces',obj.boxFaces,'Vertices',obj.boxVert+repmat(offset.',8,1),...
                'FaceColor','none', 'EdgeColor',[0 0.7 0]);
            hold off 
        end
        
        function setupBox(obj)
            obj.boxVert=zeros(8,3);
            c=[obj.problem.em obj.problem.ep];
            for s1=0:1
                for s2=0:1
                    for s3=0:1
                        obj.boxVert(4*s1+2*s2+s3+1,:)=[c(1,s1+1), c(2,s2+1), c(3,s3+1)];
                    end
                end
            end
            
            %cube faces
            obj.boxFaces=...
               [1 2 6 5;
                3 4 8 7;
                2 4 3 1;
                6 8 7 5;
                4 2 6 8;
                3 1 5 7];
        end
        
        function update(obj,visualAxes,tn)
            axes(visualAxes);
            
            offset=obj.problem.getOffset(obj.l(obj.T(tn)), obj.hCoords(obj.T(tn)));
            C=getPolygon(obj.problem.vBasis,obj.problem.vOrt, obj.problem.em+offset,obj.problem.ep+offset);
            obj.movingSet.XData=C(1,:);
            obj.movingSet.YData=C(2,:);
            obj.movingSet.ZData=C(3,:);
            
            obj.solutionMarker.XData=squeeze(obj.Y(1,tn,:));
            obj.solutionMarker.YData=squeeze(obj.Y(2,tn,:));
            obj.solutionMarker.ZData=squeeze(obj.Y(3,tn,:));
            
            set(obj.box,'Vertices',obj.boxVert+repmat(offset.',8,1));
        end        
    end    
end

