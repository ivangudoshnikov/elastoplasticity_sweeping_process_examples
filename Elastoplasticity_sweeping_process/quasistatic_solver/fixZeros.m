%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

function M1 = fixZeros(M)
[m,n]=size(M);
M1=M;
for i=1:m
    for j=1:n
        if abs(M1(i,j))<1e-14
            M1(i,j)=0;
        end
    end
end



end

