%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

classdef ShowV2D<VisualRepresInterface
    %%shows the moving set in 3d
    properties
        %visual elements
        solutionMarker,movingSet;
        
        
       
        T,problem,hCoords,l;
        %solutions data:
        %1st dim - components
        %2nd dim - different times
        %3rd dim - different solutions        
        %agreement: size(Y,1)=problem.m, size(Y,2)==length(T)
        Y;        
    end
    
    methods
        function obj=ShowV2D(problem,hCoords, l,T,Y)
            obj.T=T;
            obj.Y=Y;
            obj.problem=problem;
            obj.hCoords=hCoords;
            obj.l=l;
        end
        
        function initialDrawing(obj,visualAxes,tn)  
            axes(visualAxes);
            %drawing axis arrows
            hold on
            quiver([0,0],[0,0],[1,0],[0,1],'-k','AutoScale','off');
                        
            %drawing piston-vectors
            pZeros=zeros(1,size(obj.problem.pistonsV,2));
            quiver(pZeros,pZeros,3*obj.problem.pistonsV(1,:),3*obj.problem.pistonsV(2,:),'-r','AutoScale','off');
            %rotate3d on;
            
            %initial drawing of the moving set
            offset=obj.problem.getOffset(obj.l(obj.T(tn)), obj.hCoords(obj.T(tn)));
            vert=getPolygon2(obj.problem.vBasis,obj.problem.em+offset,obj.problem.ep+offset);

            obj.movingSet=patch(vert(:,1).',vert(:,2).','blue');
            
            %initial drawing of solution
            
            yV=zeros(2,size(obj.Y,3));
            for i=1:size(obj.Y,3)
                yV(:,i)=obj.problem.vP*obj.Y(:,tn,i);
            end            
            obj.solutionMarker=plot(yV(1,:),yV(2,:),...
                'o','MarkerFaceColor','red','Color','red','LineStyle', 'none');
            uistack(obj.solutionMarker,'top');
            hold off            
        end
        function update(obj,visualAxes,tn)
            axes(visualAxes);
            
            delete(obj.movingSet);
            offset=obj.problem.getOffset(obj.l(obj.T(tn)), obj.hCoords(obj.T(tn)));
            vert=getPolygon2(obj.problem.vBasis,obj.problem.em+offset,obj.problem.ep+offset);

            hold on
            obj.movingSet=patch(vert(:,1).',vert(:,2).','blue');
            uistack(obj.solutionMarker,'top');
            
            yV=zeros(2,size(obj.Y,3));
            for i=1:size(obj.Y,3)
                yV(:,i)=obj.problem.vP*obj.Y(:,tn,i);
            end
            set(obj.solutionMarker,{'XData','YData'},{yV(1,:),yV(2,:)});
            hold off
        end
        
    end
    
end
