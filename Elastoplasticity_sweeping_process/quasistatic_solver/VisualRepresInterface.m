%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

classdef VisualRepresInterface<handle
    %an interface for things to show in axes 
    properties
    end
    
    methods(Abstract)
        initialDrawing(obj,visualAxes,tn)
        update(obj,visualAxes,tn)
    end    
end

