%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}


function x=solveLin(A,b)
tol=1e-13;

x=A\b;

errNorm=norm(A*x-b,1);
if errNorm>tol
    error(strcat('Linear system failed! Error magnitude: ',...
        num2str(errNorm)));
end

end

