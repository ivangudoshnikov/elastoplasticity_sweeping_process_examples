%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

function p = periodic(t,Tby2)
%from 0 to 1 on phase 0 to Tby2, from 1 to zero on phase Tby2 to 2*Tby2,
%etc.

t1=mod(t,Tby2);
if mod(t,Tby2*2)<Tby2
    p=t1/Tby2;
else
    p=1-t1/Tby2;
end
end

