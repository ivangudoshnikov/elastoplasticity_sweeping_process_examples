%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

classdef PlayerGUI<handle
    %animation window gui
    properties
        %gui elements
        fig, axes1;
        editRate, lblFrame;
        btnRewind, btnPlay, btnPause;
        btnTplus, btnTminus;
        %T data
        framesAmount;
        T;
        %current frame
        tn; 
        running;
        %visual representation to draw on the axis1
        visualRepres1;
        %timer to do animaton
        timerPeriod=0.05;%seconds
        guiTimer;
    end
    
    methods
        function obj=PlayerGUI(name,axesLimits,T,visualRepres)
            obj.T=T;
            
            obj.framesAmount=size(T,2);
            obj.tn=1;
            obj.running=false;
            
            obj.fig = figure('Name',name,'Visible','off',...
                'units','normalized','outerposition',[0,0.1,1,0.9],...
                'NumberTitle','off','MenuBar','none','ToolBar','figure',...
                'KeyPressFcn',@keyboardHandler,'CloseRequestFcn',{@winCloseHandler,obj});
            set(obj.fig,'units','pixels');
            obj.fig.UserData=obj;
            
            uicontrol(obj.fig,'Style','text','String','Speed:',...
                'Position',[10,30,40,20],'HorizontalAlignment','right');
            obj.editRate=uicontrol(obj.fig,'Style','edit','String',num2str(ceil(obj.framesAmount/1000)),'Position',[50,32,30,20]);
            
            uicontrol(obj.fig,'Style','text','String','Frame:',...
                'Position',[100,30,40,20],'HorizontalAlignment','right');
            obj.lblFrame= uicontrol(obj.fig,'Style','text','String',num2str(obj.tn),...
                'Position',[140,30,40,20],'HorizontalAlignment','center');
            uicontrol(obj.fig,'Style','text','String',strcat(':',num2str(obj.framesAmount)),...
                'Position',[180,30,40,20],'HorizontalAlignment','left');
            
            obj.btnTplus=uicontrol('Style','pushbutton',...
                'Position',[220,30+3, 20, 20],...
                'String','+','Callback',{@tPlusHandler,obj});
            obj.btnTminus=uicontrol('Style','pushbutton',...
                'Position',[240,30+3, 20, 20],...
                'String','-','Callback',{@tMinusHandler,obj});
            
            
            obj.btnRewind=uicontrol('Style','pushbutton','String', '|<','Position', [100,10,40,20],'Callback',{@rewindHandler,obj});       
            obj.btnPlay=uicontrol('Style','pushbutton','String', '>','Position', [140,10,40,20],'Callback',{@playHandler,obj});       
            obj.btnPause=uicontrol('Style','pushbutton','String', '| |','Position', [180,10,40,20],'Callback',{@pauseHandler,obj});       
            
            
            obj.axes1=axes('Units','pixels',...
                'outerposition',[0,100,obj.fig.Position(3),obj.fig.Position(4)-100],...
                'PlotBoxAspectRatio',[1,1,1]);
            
            
            axes(obj.axes1);
            axis manual;
            axis(axesLimits);
            
            obj.visualRepres1=visualRepres;
            obj.visualRepres1.initialDrawing(obj.axes1,obj.tn);
            
            set(obj.fig,'Visible','on');
            drawnow;
            
            obj.guiTimer=timer('BusyMode','drop','ExecutionMode','fixedRate',...
                'Period',obj.timerPeriod,'StartDelay',obj.timerPeriod,'TimerFcn',{@timerHandler,obj},...
                'ErrorFcn',@(src,event)delete(src));
        end
        
        
        function updateDraw(obj)
            obj.lblFrame.String=num2str(obj.tn);
            obj.visualRepres1.update(obj.axes1,obj.tn);
            drawnow;
        end
        
        function rewind(obj)
            obj.tn=1;
            obj.updateDraw;
        end
        
        function play(obj)
            if ~obj.running 
                start(obj.guiTimer);
                obj.running=true;
            end
            
            
        end
        function pause(obj)
            if obj.running
                stop(obj.guiTimer);
                obj.running=false;   
            end            
                     
        end
        function run(obj)
            if obj.running
                if obj.tn<obj.framesAmount
                    obj.timePlus;
                else
                    obj.pause;
                end
            end    
        end
        
        function timePlus(obj)
            [rate,status]=str2num(obj.editRate.String);
            if status
                rate=ceil(rate);
                if obj.tn+rate<=obj.framesAmount
                    obj.tn=obj.tn+rate;
                else
                    obj.tn=obj.framesAmount;
                end
                obj.updateDraw;
            end
        end
        
        function timeMinus(obj)
            [rate,status]=str2num(obj.editRate.String);
            if status
                rate=ceil(rate);
                if obj.tn-rate>=1
                    obj.tn=obj.tn-rate;
                else
                    obj.tn=1;
                end
                obj.updateDraw;
            end
        end
        
    end
    
end

function keyboardHandler(obj,data)
%KEYBOARDHANDLER keyboard callback
%disp(data.Key);
    switch data.Key
        case 'rightarrow'
            
            obj.UserData.timePlus;
        case 'leftarrow'
            obj.UserData.timeMinus;
    end
end

function rewindHandler(src,event,obj)
    obj.rewind;
end
function playHandler(src,event,obj)
obj.play;
end
function pauseHandler(src,event,obj)
obj.pause;
end
function timerHandler(src,event,obj)
obj.run;
end
function tPlusHandler(src,event,obj)
    obj.timePlus;
end
function tMinusHandler(src,event,obj)
    obj.timeMinus;
end
function winCloseHandler(src,data,obj)
 if isvalid(obj.guiTimer) 
     delete(obj.guiTimer);
 end 
 delete(src);
end

