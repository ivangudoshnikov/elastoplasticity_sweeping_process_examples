classdef SpringsSetupVisual<VisualRepresInterface
    properties
        problem,T,hCoords,l;
        Y,E,P,X;
        %arrays containng the data about the drawings
        nodedots, springs, pistons, vertlines;
        %parameters of drawing
        nodeConstr;
        nodeConstrVal;
        defaultNodesX;
        extForces;
        
        %visual elements
        nodesMarker,vertlinesLine,pistonsLine;
        extForcesLine,extForcesArrowheads;
        springsElementVisual;
    end
    
    methods
        function obj=SpringsSetupVisual(problem,hCoords,l,T,Y,E,P,X,...
                nodeConstr,nodeConstrVal,defaultNodesX,...
                nodedots,springs,pistons,vertlines,extForces)
            obj.problem=problem;
            obj.nodeConstr=nodeConstr;
            obj.nodeConstrVal=nodeConstrVal;
            obj.defaultNodesX=defaultNodesX;
            obj.nodedots=nodedots;
            obj.springs=springs;
            obj.pistons=pistons;
            obj.vertlines=vertlines;
            obj.extForces=extForces;
            obj.T=T;
            obj.Y=Y;
            obj.E=E;
            obj.P=P;
            obj.X=X;
            obj.hCoords=hCoords;
            obj.l=l;
            
        end
        
        function initialDrawing(obj,visualAxes,tn)
            %visualAxes - handle to the axis where the setup must be drawn
            %e - elastic elongation at the moment
            %p - plastic elongation at the moment
            %x - total elongation at the moment
            %f - external forces at the nodes at the moment
            
            axes(visualAxes);
            set(visualAxes,'XColor','black','YColor','none','ZColor','none','XAxisLocation','origin',...
                'XTick',visualAxes.XLim(1):visualAxes.XLim(2),'XTickLabel',{})
            nodesCoords=getNodesCoords(obj.defaultNodesX,obj.problem.D,obj.X(:,tn),obj.nodeConstr,obj.nodeConstrVal);
            hold on
            
            [xdata,ydata]=obj.getNodesDotsCoords(nodesCoords);                        
            obj.nodesMarker=plot(xdata,ydata,...
                'o','MarkerFaceColor','blue','Color','blue','LineStyle', 'none');
            
            [xdata,ydata]=obj.getPistonsCoords(nodesCoords); 
            obj.pistonsLine=line(xdata,ydata,'Color','red','LineWidth',2);
            
            [xdata,ydata]=obj.getVertLinesCoords(nodesCoords); 
            obj.vertlinesLine=line(xdata,ydata,'Color','black','LineWidth',1);
            
            if ~isempty(obj.extForces)
                [xdata1,ydata1,xdata2,ydata2]=obj.getExtForcesCoords(nodesCoords,...
                    obj.problem.forceByH(obj.problem.hByCoords(obj.hCoords(obj.T(tn)))));
                obj.extForcesLine=line(xdata1,ydata1,'Color','green','LineWidth',1);
                obj.extForcesArrowheads=line(xdata2,ydata2,'Color','green','LineWidth',1);
            end
            hold off
            obj.springsElementVisual=SpringsElementVisual(visualAxes,...
                obj.defaultNodesX,obj.springs,obj.problem.D,obj.E(:,tn),obj.P(:,tn),obj.X(:,tn),nodesCoords);
            
            uistack(obj.nodesMarker,'top');
        end
        function update(obj,visualAxes,tn)
            %visualAxes - handle to the axis where the setup must be drawn
            %e - elastic elongation at the moment
            %p - plastic elongation at the moment
            %x - total elongation at the moment
            %f - external forces at the nodes at the moment
            axes(visualAxes);
            nodesCoords=getNodesCoords(obj.defaultNodesX,obj.problem.D,obj.X(:,tn),obj.nodeConstr,obj.nodeConstrVal);
            
            [xdata,ydata]=obj.getNodesDotsCoords(nodesCoords);
            set(obj.nodesMarker,'XData',xdata,'YData',ydata);
            
            if ~isempty(obj.extForces)
                [xdata1,ydata1,xdata2,ydata2]=obj.getExtForcesCoords(nodesCoords,...
                    obj.problem.forceByH(obj.problem.hByCoords(obj.hCoords(obj.T(tn)))));
                for i=1:length(obj.extForcesLine)
                    set(obj.extForcesLine(i),'XData',xdata1(:,i),'YData',ydata1(:,i));
                    set(obj.extForcesArrowheads(i),'XData',xdata2(:,i),'YData',ydata2(:,i));
                end
            end
            
            [xdata,ydata]=obj.getPistonsCoords(nodesCoords); 
            for i=1:length(obj.pistons)
                obj.pistonsLine(i).XData=xdata(:,i);
                obj.pistonsLine(i).YData=ydata(:,i);
            end
            
            [xdata,ydata]=obj.getVertLinesCoords(nodesCoords); 
            for i=1:length(obj.vertlines)
                obj.vertlinesLine(i).XData=xdata(:,i);
                obj.vertlinesLine(i).YData=ydata(:,i);
            end            
                        
            obj.springsElementVisual.update(obj.E(:,tn),obj.P(:,tn),obj.X(:,tn),nodesCoords);
            
        end
        
        function [xdata1,ydata1,xdata2,ydata2]=getExtForcesCoords(obj,nodesCoords,f)
            xdata1=repmat(nodesCoords.',2,1);
            xdata2=repmat(nodesCoords.',3,1);
            ydata1=repmat(obj.extForces.y.',2,1);
            ydata2=repmat(obj.extForces.y.',3,1);
            
            %if we need to draw an actual arrow
            for i=1:length(nodesCoords)
                if abs(f(i))>1e-13
                    xdata1(2,i)=xdata1(2,i)+obj.extForces.k*f(i);
                    
                    xdata2(1,i)=xdata2(1,i)+obj.extForces.k*f(i)-sign(f(i))*obj.extForces.aw;
                    xdata2(2,i)=xdata2(2,i)+obj.extForces.k*f(i);
                    xdata2(3,i)=xdata2(3,i)+obj.extForces.k*f(i)-sign(f(i))*obj.extForces.aw;
                    ydata2(1,i)=ydata2(1,i)-obj.extForces.ah;
                    ydata2(3,i)=ydata2(3,i)+obj.extForces.ah;
                end
            end
            
        end
        
        function [xdata,ydata]=getVertLinesCoords(obj,nodesCoords)
           xdata=zeros(2,length(obj.vertlines));
           ydata=zeros(2,length(obj.vertlines));
           for i=1:length(obj.vertlines)
               xdata(1,i)=nodesCoords(obj.vertlines(i).node);            
               xdata(2,i)=xdata(1,i);
               ydata(1,i)=obj.vertlines(i).y1;
               ydata(2,i)=obj.vertlines(i).y2;
           end    
        end
                
        function [xdata,ydata]=getPistonsCoords(obj,nodesCoords)
           xdata=zeros(2,length(obj.pistons));
           ydata=zeros(2,length(obj.pistons));
           for i=1:length(obj.pistons)
               xdata(1,i)=nodesCoords(obj.pistons(i).lnode);            
               xdata(2,i)=nodesCoords(obj.pistons(i).rnode);
               ydata(1,i)=obj.pistons(i).y;
               ydata(2,i)=ydata(1,i);
           end
        end
        
        function [xdata,ydata]=getNodesDotsCoords(obj,nodesCoords)
            xdata=zeros(length(obj.nodedots),1);
            ydata=zeros(length(obj.nodedots),1);
            for i=1:length(obj.nodedots)
                xdata(i)=nodesCoords(obj.nodedots(i).node);
                ydata(i)=obj.nodedots(i).y;
            end
        end
    end
    
end


function xi=getNodesDisplacements(D,x,nodeConstr,nodeConstrVal)
%NOT VECTORIZED YET!
%x - total elongation
%nodeConstr - additional constratint on nodes to solve the system
%nodeConstrVal - its value
D1=[D; nodeConstr];
b=[x; nodeConstrVal];
xi=solveLin(D1,b);
end

function nodesCoords=getNodesCoords(defaultNodesX,D,x,nodeConstr,nodeConstrVal)
%xi0 - default nodes coords
%x - total elongation
%nodeConstr - additional constratint on nodes to solve the system
%nodeConstrVal - its value
    nodesCoords=defaultNodesX+getNodesDisplacements(D,x,nodeConstr,nodeConstrVal);
end


