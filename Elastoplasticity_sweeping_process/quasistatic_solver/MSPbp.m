%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

classdef MSPbp<handle
    %Moreau Sweeping Process formed by a box and planes
    
    properties
        H, lb,ub, Aeq;
    end
    
    methods
        function obj=MSPbp(H,lb,ub,Aeq)
        obj.H=H; %scalar product matrix
        obj.lb=lb; %lower corner of the box
        obj.ub=ub; %upper corner of the box
        obj.Aeq=Aeq;%plane defining operator: V=Ker Aeq
        end
        
        function x1=catchup(obj, x, t, c)
            %c - box translation vector at t
            k=size(obj.Aeq);
            opts = optimoptions('quadprog','Algorithm','active-set','Display','off');
            [x1,val,exitflag]=quadprog(obj.H,-obj.H*x,[],[],obj.Aeq,zeros(k(1),1),...
                obj.lb+c,obj.ub+c,[],opts);
            if exitflag~=1
                error(strcat('cannot optimize, fval: ',num2str(val),...
                    ', exitflag: ',num2str(exitflag)))
            end
        end
        
    end
    
end

