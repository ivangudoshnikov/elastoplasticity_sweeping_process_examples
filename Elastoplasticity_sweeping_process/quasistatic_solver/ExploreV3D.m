%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

classdef ExploreV3D<handle
    %explore tool
    properties
        %gui elements
        figure3D, axes3D;
        panel, springsList;
        lblC,lblA,lblConstr;
        btnCatchup,btnExport;
        btnPC,btnPA, btnMC, btnMA;
        
        edtDt,edtAmp,edtPer, edtNPer;
        %graph elements
        movingSet, solution3Line,pistonVector, catchupLine;
        prism;
        %prism description
        prismVert,prismFaces;
        
        %chosen spring
        sn;
        %drawn solution
        X;
        %incative constr for the drawn soluton
        I;
        %problem data
        problem;        
        %opacity of the moving set
        setOpacity=0.3;
        %draw prism?
        drawPrism;
        %draw it arter sqrt(A)? NOT IMPLEMENTED YET
        sqrtSpace;       
        
    end
    
    methods
        function obj=ExploreV3D(axes3Dlimits,problem,drawPrism,sqrtSpace)
            %ASSUMPTIONS:
            %dim V=3
            %only 1 piston
            obj.problem=problem;
            obj.drawPrism=drawPrism;
            
            obj.sqrtSpace=sqrtSpace; % not yet implemented
            
            obj.createUI;
            
            axes(obj.axes3D);
            axis manual;
            axis(axes3Dlimits);
            
            %drawing axis arrows
            hold on
            quiver3([0,0,0],[0,0,0],[0,0,0],[1,0,0],[0,1,0],[0,0,1],'-k','AutoScale','off');
            
            %initial draw of piston-vector;
            obj.pistonVector=quiver3(0,0,0,3*obj.problem.pistonsV(1),3*obj.problem.pistonsV(2),3*obj.problem.pistonsV(3),...
                '-r','AutoScale','off');
            hold off
            
            
            %create analytic solution line
            obj.solution3Line=line();
            set(obj.solution3Line, 'Color',[0.2,0.01,0.01]);
            set(obj.solution3Line, 'LineWidth',2);
            
            %rotate3d on;
            
            obj.updateModel;
            
            obj.updateDraw;
            obj.updateUI;
            
            set(obj.figure3D,'Visible','on');
        end
        
        function createUI(obj)
            obj.figure3D = figure('Name','Explore 3D Moreau sweeping process','Visible','off',...
                'units','normalized','outerposition',[0,0.1,1,0.9],...
                'NumberTitle','off','MenuBar','none','ToolBar','figure','KeyPressFcn',@keyboardHandler);
            
            set(obj.figure3D,'units','pixels');
            obj.figure3D.UserData=obj;
            
            
            obj.panel= uipanel(obj.figure3D,'units','pixels','Position',[10,10,200,obj.figure3D.Position(4)-50]);
            
            uicontrol('Style','text','Parent', obj.panel,...
                'Position',[10, obj.panel.Position(4)-30 100 20],...
                'String','Spring #:');
            obj.springsList= uicontrol('Style', 'listbox',...
                'Parent', obj.panel,...
                'String', 1:obj.problem.m,...
                'Position', [10, obj.panel.Position(4)-110, 70, 80],...
                'Callback', @sprilgsListCallback,'KeyPressFcn',@keyboardHandler);
            obj.sn=int8(obj.springsList.Value);
            
            uicontrol('Style','text','Parent', obj.panel,...
                'Position',[90, obj.panel.Position(4)-50, 20, 20],...
                'String','c+:');
            obj.lblC=uicontrol('Style','text','Parent', obj.panel,...
                'Position',[110, obj.panel.Position(4)-50, 30, 20]);
            obj.btnPC=uicontrol('Style','pushbutton','Parent', obj.panel,...
                'Position',[140, obj.panel.Position(4)-50+3, 20, 20],...
                'String','+','Callback',{@cPlusHandler,obj});
            obj.btnMC=uicontrol('Style','pushbutton','Parent', obj.panel,...
                'Position',[160, obj.panel.Position(4)-50+3, 20, 20],...
                'String','-','Callback',{@cMinusHandler,obj});
            
            
            uicontrol('Style','text','Parent', obj.panel,...
                'Position',[90, obj.panel.Position(4)-70, 20, 20],...
                'String','a:');
            obj.lblA=uicontrol('Style','text','Parent', obj.panel,...
                'Position',[110, obj.panel.Position(4)-70, 30, 20]);
            obj.btnPA=uicontrol('Style','pushbutton','Parent', obj.panel,...
                'Position',[140, obj.panel.Position(4)-70+3, 20, 20],...
                'String','+','Callback',{@aPlusHandler,obj});
            obj.btnMA=uicontrol('Style','pushbutton','Parent', obj.panel,...
                'Position',[160, obj.panel.Position(4)-70+3, 20, 20],...
                'String','-','Callback',{@aMinusHandler,obj});
            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            uicontrol('Style','text','Parent', obj.panel,...
                'Position',[30, obj.panel.Position(4)-140, 100, 20],...
                'String','Active constraints:');
            obj.lblConstr=uicontrol('Style','text','Parent', obj.panel,...
                'Position',[30, obj.panel.Position(4)-220, 100, 80]);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            uicontrol('Style','text','Parent', obj.panel,...
                'Position',[5, obj.panel.Position(4)-250-2, 30, 20],...
                'String','dt:');
            obj.edtDt=uicontrol('Parent', obj.panel,'Style','edit','Parent', obj.panel,...
                'String','0.01','Position',[35,obj.panel.Position(4)-250,30, 20]);
            
            uicontrol('Style','text','Parent', obj.panel,...
                'Position',[65, obj.panel.Position(4)-250-2, 30, 20],...
                'String','amp:');
            obj.edtAmp=uicontrol('Parent', obj.panel,'Style','edit','Parent', obj.panel,...
                'String','5','Position',[95,obj.panel.Position(4)-250,30, 20]);
            
            obj.btnCatchup=uicontrol('Style','pushbutton','Parent', obj.panel,...
                'Position',[130, obj.panel.Position(4)-250, 60, 20],...
                'String','Catchup','Callback',@btnCatchupCallback);
            
            uicontrol('Style','text','Parent', obj.panel,...
                'Position',[5, obj.panel.Position(4)-280-2, 30, 20],...
                'String','per:');
            obj.edtPer=uicontrol('Parent', obj.panel,'Style','edit','Parent', obj.panel,...
                'String','2','Position',[35,obj.panel.Position(4)-280,30, 20]);
            
            uicontrol('Style','text','Parent', obj.panel,...
                'Position',[65, obj.panel.Position(4)-280-2, 30, 20],...
                'String','# per:');
            obj.edtNPer=uicontrol('Parent', obj.panel,'Style','edit','Parent', obj.panel,...
                'String','5','Position',[95,obj.panel.Position(4)-280,30, 20]);
            
            obj.btnExport=uicontrol('Style','pushbutton','Parent', obj.panel,...
                'Position',[130, obj.panel.Position(4)-280, 60, 20],...
                'String','Export','Callback',@btnExportCallback);
            
            obj.axes3D=axes('Units','pixels',...
                'outerposition',[100,100,obj.figure3D.Position(3),obj.figure3D.Position(4)-100],...
                'PlotBoxAspectRatio',[1,1,1]);
            
        end
        
        function updateDraw(obj)
            axes(obj.axes3D);
            
            delete(obj.catchupLine);
            delete(obj.movingSet);
            delete(obj.prism);
            
            hold on
            %drawing the moving set
            [vert,faces]=getPolyhedron(obj.problem.vBasis,obj.problem.em,obj.problem.ep);
            obj.movingSet=patch('Faces',faces,'Vertices',vert,'FaceColor','blue','FaceAlpha',obj.setOpacity);
            
            %drawing the prism
            if obj.drawPrism
                obj.prism=patch('Faces',obj.prismFaces,'Vertices',obj.prismVert,'FaceColor','yellow','FaceAlpha',0.3);
            end
            hold off
            
            %piston-vector
            set(obj.pistonVector,{'UData','VData','WData','AutoScale'},...
                {3*obj.problem.pistonsV(1),3*obj.problem.pistonsV(2),...
                3*obj.problem.pistonsV(3),'off'});
            
            %event-based solution plot
            x=obj.problem.vP*obj.X;
            set(obj.solution3Line,{'XData','YData','ZData'},{x(1,:),x(2,:),x(3,:)});
            
            drawnow;
        end
        
        function updateModel(obj)
            [obj.X,V,obj.I]=obj.problem.getSolutionStages(4); %4 for prism
            [obj.prismVert, obj.prismFaces]=obj.getPrism;
        end
        
        function updateUI(obj)
            set(obj.lblC,'String',num2str(obj.problem.ep(obj.sn)));
            set(obj.lblA,'String',num2str(obj.problem.a(obj.sn)));
            
            %active constraints per stage list
            strI=cell(1,size(obj.I,2));
            for i=1:size(obj.I,2)-1
                ac=setdiff(1:obj.problem.m,union(obj.I{i},obj.I{i+1}));
                if size(ac,2)==0
                    strI{i}='[]';
                elseif size(ac,2)==1
                    strI{i}=strcat('[',mat2str(ac),']');
                else
                    strI{i}=mat2str(ac);
                end
            end
            set(obj.lblConstr,'String',strI);
        end
        
        function catchup(obj)
            delete(obj.catchupLine);
            
            dt=str2double(obj.edtDt.String);
            amp=str2double(obj.edtAmp.String);
            if (~isnan(dt))&&(~isnan(amp))
                T=0:dt:amp;
                e0=zeros(obj.problem.m,1);
                p0=zeros(obj.problem.m,1);
                
                hCoords = @(t)[0;0];
                l=@(t)t;
                
                [Y,E]=obj.problem.solveElastic(T,e0,hCoords,l);
                %[P,X]=obj.problem.solvePlastic(T,E,p0,l,'active-set');
                %obj.problem.verifySolution(T,E,P,l,hCoords,1e-9);
                
                vE=obj.problem.vP*E;
                obj.catchupLine=line(vE(1,:),vE(2,:),vE(3,:),'Color',[1,1,0],...
                    'LineWidth',3);
            end
        end
        
        function export(obj)
            disp(strcat('a: ',num2str(obj.problem.a)));
            disp(strcat('sp: ',num2str((diag(obj.problem.a)*obj.problem.ep).')));
            disp(strcat('ep: ',num2str(obj.problem.ep.')));
            
            dt=str2double(obj.edtDt.String);
            Amp=str2double(obj.edtAmp.String);
            Per=str2double(obj.edtPer.String);
            NPer=str2double(obj.edtNPer.String);
            
            if (~isnan(dt))&&(~isnan(Amp))&&(~isnan(Per))&&(~isnan(NPer))
                T=0:dt:Per*NPer;
                e0=[0;0;0;0;0];
                p0=[0;0;0;0;0];
                
                hCoords = @(t)[0;0];
                l=@(t)(piecewiseLinPer(t,Per,Amp));
                
                tol=1e-9;
                
                [Y,E]=obj.problem.solveElastic(T,e0,hCoords,l);
                [P,X]=obj.problem.solvePlastic(T,E,p0,l,'active-set',tol);
                obj.problem.verifySolution(T,E,P,l,hCoords,tol);
                
                figure('Name','Elastic parts');
                p1=plot(T,E);
                legend('show');
                
                figure('Name','Solution of the sweeping process');
                p2=plot(T,Y);
                legend('show');
                
                periodDifferencePlot(T,E,round(Per/dt),round(Per/dt)+1)
                
               %setup a visual appearence of the springs system

               %additional constratint on nodes to solve the system for nodes coords
               nodeConstr=[1 0 0 0];
               %value of the constr
               nodeConstrVal=0;
               defaultNodesX=[-12; -4; 4; 12];
               
               %y's used in the visualization of the springs
               y1=4;
               y2=3;
               y3=2;
               y4=1;
               
               %drawing markers at joints, each has: corresponding node, height
               nodedots=[struct('node',1,'y',y1),...
                   struct('node',3,'y',y1),...
                   struct('node',1,'y',y2),...
                   struct('node',2,'y',y2),...
                   struct('node',3,'y',y2),...
                   struct('node',4,'y',y2),...
                   struct('node',2,'y',y3),...
                   struct('node',4,'y',y3),...
                   struct('node',1,'y',y4),...
                   struct('node',4,'y',y4)... 
                   ];
                %                                    
                %                     #=ek          _____
                %                  h _/\  /\  _____|   __|__    aw,ah - arrowhead height&width
                %                   l1  \/  \/l2 l3    l4 l5

               springs=[struct('lnode',1,'rnode',2,'defElastLen',4,'y',y2,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
                        struct('lnode',1,'rnode',3,'defElastLen',8,'y',y1,   'h',0.2,'ek',6, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
                        struct('lnode',2,'rnode',3,'defElastLen',4,'y',y2,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
                        struct('lnode',2,'rnode',4,'defElastLen',8,'y',y3,   'h',0.2,'ek',6, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
                        struct('lnode',3,'rnode',4,'defElastLen',4,'y',y2,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1)...
                       ];
               
               pistons=[struct('lnode',1,'rnode',4,'y',y4)];
               
               vertlines=[struct('node',1,'y1',y1,'y2',y4),...
                   struct('node',2,'y1',y2,'y2',y3),...
                   struct('node',3,'y1',y1,'y2',y2),...
                   struct('node',4,'y1',y2,'y2',y4)...
                   ];
               extForces=[];
               springsSetupVisual=SpringsSetupVisual(obj.problem,hCoords,l,T,Y,E,P,X,...
                   nodeConstr,nodeConstrVal,defaultNodesX,...
                   nodedots,springs,pistons,vertlines,extForces);
               showV3D=ShowV3D(obj.problem,hCoords, l,T,Y);
               
               axes3Dsize=[-5 5 -5 5 -5 5];
               axesSpringsSize=[-12 15 -3 8];
               PlayerGUI2axes('Sweeping process in 3D',axes3Dsize,axesSpringsSize,T,showV3D,springsSetupVisual);
               
            end
            
        end
        
        function [vert, faces]=getPrism(obj)
            %generating vectors
            dX=zeros(3,3);
            for i=1:3
                dX(:,i)=obj.problem.vP*(obj.X(:,i+1)-obj.X(:,i));
            end
            
            %vertices
            vert=zeros(3,8);
            for s1=0:1
                for s2=0:1
                    for s3=0:1
                        vert(:,4*s1+2*s2+s3+1)=(2*s1-1)*dX(:,1)+...
                            (2*s2-1)*dX(:,2)+(2*s3-1)*dX(:,3);
                    end
                end
            end
            
            vert=vert.';
            %cube faces
            faces=[1 2 6 5;
                3 4 8 7;
                2 4 3 1;
                6 8 7 5;
                4 2 6 8;
                3 1 5 7];
            
        end
        
        function cPlus(obj)
            newEP=obj.problem.ep;
            newEP(obj.sn)=newEP(obj.sn)+0.05;
            obj.problem.setBoundsMSP(-newEP,newEP);
            
            obj.updateModel;
            obj.updateUI;
            obj.updateDraw;
        end
        
        function cMinus(obj)            
            newEP=obj.problem.ep;
            if newEP(obj.sn)>0.05
                newEP(obj.sn)=newEP(obj.sn)-0.05;
            end
            obj.problem.setBoundsMSP(-newEP,newEP);
            
            obj.updateModel;
            obj.updateUI;
            obj.updateDraw;            
        end
        
        function aPlus(obj)
            newEP=obj.problem.ep;
            newA=obj.problem.a;
            newA(obj.sn)=newA(obj.sn)+0.05;
            obj.problem.setAV(newA);
            obj.problem.setBoundsMSP(-newEP,newEP);
            
            obj.updateModel;
            obj.updateUI;
            obj.updateDraw;            
        end
        
        function aMinus(obj)
            newEP=obj.problem.ep;
            newA=obj.problem.a;
            if newA(obj.sn)>0.05
                newA(obj.sn)=newA(obj.sn)-0.05;
            end
            obj.problem.setAV(newA);
            obj.problem.setBoundsMSP(-newEP,newEP);
            
            obj.updateModel;
            obj.updateUI;
            obj.updateDraw;  
        end
    end
    
end


function sprilgsListCallback(obj,data)
obj.Parent.Parent.UserData.sn=int8(obj.Value);
obj.Parent.Parent.UserData.updateUI;
end

function btnCatchupCallback(obj,data)
obj.Parent.Parent.UserData.catchup;
end

function btnExportCallback(obj,data)
obj.Parent.Parent.UserData.export;
end

function keyboardHandler(obj,data)
%KEYBOARDHANDLER keyboard callback
%disp(data.Key);

expl=get(gcbf, 'UserData');
switch data.Key
    case {'d','rightarrow'}
        expl.cPlus;
    case {'a','leftarrow'}
        expl.cMinus;
    case {'w','uparrow'}
        expl.aPlus;
    case {'s','downarrow'}
        expl.aMinus;
end
end

function cPlusHandler(src,event,obj)
obj.cPlus;
end
function cMinusHandler(src,event,obj)
obj.cMinus;
end
function aPlusHandler(src,event,obj)
obj.aPlus;
end
function aMinusHandler(src,event,obj)
obj.aMinus;
end


