%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

function indices = sortVertices(V,basis)
%V - each column is a vertex
%basis - each row is a vector from orthonormal basis

%finding center
center=mean(V,2);

[~,n]=size(V);
%finding coordinates in basis, assuming it is orthonormal
Q=zeros(1,n);
for i=1:n
    x1=basis(1,:)*(V(:,i)-center);
    x2=basis(2,:)*(V(:,i)-center);
    Q(i)=atan2(x1,x2);
end

[~,indices]=sort(Q);
end

