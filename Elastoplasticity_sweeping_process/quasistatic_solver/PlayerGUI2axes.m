%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}
classdef PlayerGUI2axes<PlayerGUI
    %animation window with 2 axes side-by-side
    properties
        %additional gui element
        axes2;
        visualRepres2;
    end
    
    methods
        function obj=PlayerGUI2axes(name,axes1Limits,axes2Limits,T,visualRepres1,visualRepres2)
            obj@PlayerGUI(name,axes1Limits,T,visualRepres1);
            set(obj.axes1,'outerposition',[0,100,2*obj.fig.Position(3)/3,obj.fig.Position(4)-100]);
            
            obj.axes2=axes('Units','pixels',...
                'outerposition',[obj.fig.Position(3)/3,100,2*obj.fig.Position(3)/3,obj.fig.Position(4)-100],...
                'PlotBoxAspectRatio',[1,1,1]);
           
            axes(obj.axes2);
            axis manual
            axis(axes2Limits);
            
            obj.visualRepres2=visualRepres2;
            obj.visualRepres2.initialDrawing(obj.axes2,obj.tn);
            drawnow;
        end   
        function updateDraw(obj)
           obj.visualRepres2.update(obj.axes2,obj.tn);
           updateDraw@PlayerGUI(obj);
        end
    end
    
end

