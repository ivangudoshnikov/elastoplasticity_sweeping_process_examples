%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}


function [newFaces,maxFaceSize] = detriangulate(vert, faces)
%returns cell array of indices of each face

tol=1e-14;

nTriangles=size(faces,1);

%finding normals
trNormals=zeros(nTriangles,3);
for i=1:nTriangles
    trNormals(i,:)=normalize(cross(vert(faces(i,2),:)-vert(faces(i,1),:),...
        vert(faces(i,3),:)-vert(faces(i,1),:)));
end


%classifying triangles by faces (groups)
group(1)=newFace(1,vert,faces,trNormals);
nFaces=1;

for i=2:nTriangles
    faceFound=false;
    for j=1:nFaces
        
        if abs(abs(dot(group(j).normal,trNormals(i,:)))-1)<tol &&...
                ~isempty(intersect(faces(i,:),group(j).vert))
            faceFound=true;
            group(j).vert=[group(j).vert faces(i,:)];
            break;
        end
        
    end
    
    if ~faceFound
        nFaces=nFaces+1;
        group(nFaces)=newFace(i,vert,faces,trNormals);
    end
end

%no repeated vertices within a face. Also calculating max size.
maxFaceSize=3;
for j=1:nFaces
    group(j).vert=unique(group(j).vert);
    if size(group(j).vert,2)>maxFaceSize
        maxFaceSize=size(group(j).vert,2);
    end
end

%ordering vertices in each face
newFaces=cell(1,nFaces);

for j=1:nFaces
    %length(amount of vertices)
    l=size(group(j).vert,2);
    v=zeros(3,l);
    %extracting coordinates
    for i=1:l
        v(:,i)=vert(group(j).vert(i),:).';
    end
    sortedVert=sortVertices(v,group(j).basis);
    
    newFaces(j)= {group(j).vert(sortedVert(:))};
end

end

function xn=normalize(x)
xn=x/norm(x);
end

function nf=newFace(i,vert,faces,trNormals)

nf.normal=trNormals(i,:);
nf.vert=faces(i,:);
v1=normalize(vert(faces(i,2),:)-vert(faces(i,1),:)); %first vector of basis 
nf.basis=[v1;cross(nf.normal,v1)];
end


