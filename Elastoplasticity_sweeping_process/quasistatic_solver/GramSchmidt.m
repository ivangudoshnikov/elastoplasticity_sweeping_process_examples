%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

function [Q,R] = GramSchmidt(A)
%https://www.mathworks.com/matlabcentral/newsreader/view_thread/48300
%http://web.mit.edu/18.06/www/Essays/gramschmidtmat.pdf
[m,n] = size(A);
% compute QR using Gram-Schmidt
for j = 1:n
   v = A(:,j);
   for i=1:j-1
        R(i,j) = Q(:,i)'*A(:,j);
        v = v - R(i,j)*Q(:,i);
   end
   R(j,j) = norm(v);
   Q(:,j) = v/R(j,j);
end
end
