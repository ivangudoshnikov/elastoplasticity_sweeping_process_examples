%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

function Psorted = getPolygon(basis,ort, em,ep)
%returns vertices of the slice of the box [sm, sp] by the plane defined by ort.

boundaries=[em ep];
P=[];
%going over all edges

%k-th edge is excluded
for k=1:3
    ind=1:3;
    ind(k)=[];
    %ind are indecies of remaning edges
    for i=1:2
        for j=1:2
            v1=[0 0 0];
            v1(ind(1))=1;
            v2=[0 0 0];
            v2(ind(2))=1;
            M=[ort; v1; v2];
            if det(M)>10*eps
                b=[0;boundaries(ind(1),i); boundaries(ind(2),j)];
                x=M\b;
                if (x(k)>=boundaries(k,1))&&(x(k)<=boundaries(k,2))
                    P=[P x];
                end
            end
            
        end
    end
end
%determining amount of vertices
sz=size(P);
n=sz(2);

if n<4
    Psorted=P;
else
    %if there are more then 3 vertices we sort them according to
    %the angles with 1st vertex w.r.to the center.
    
    %preparation
    Psorted=zeros(3,n);
    I=sortVertices(P,basis.');
    for i= 1:n
        Psorted(:,i)=P(:,I(i));
    end
end

end

