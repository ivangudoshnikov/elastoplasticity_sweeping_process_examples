%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

function periodDifferencePlot(T,X,framePeriod,startingFrame)
Y=zeros(size(X,1),size(X,2)-framePeriod);
for i=1:size(X,2)-framePeriod
    Y(:,i)=X(:,i+framePeriod)-X(:,i);
end
figure('Name','1-period difference');
T1=T(startingFrame:end);
Y1=Y(:,startingFrame-framePeriod:end);
plot(T1,Y1);
legend('show');
end

