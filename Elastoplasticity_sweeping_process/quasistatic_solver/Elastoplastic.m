%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

classdef Elastoplastic<handle
    %representation of an elaastoplastic problem
    properties
        D;%geometry of the lattice
        m;%amount of springs
        n;%amount of nodes
        dimV, dimU; %dimensions for convinience;
        L;%geometry of pistons
        pistonsH; %columns are full-space coords of piston-vectors
        pistonsV;%columns are vBasis-coords of piston-vectors in V
        a;% stiffnesses
        em,ep;%minimal and maximal elastic enlongations
        imDOrt% ImD = Ker imDOrt
        uBasis;%columns form basis of U
        vBasis;%columns form basis of V
        uP;%projector on U (result in uBasis coords)
        vP;%projector on V (result in vBasis coords)
        uOrt;% U=Ker uOrt
        vOrt;% V=Ker vOrt
        msp;
    end
    
    methods            
        function obj=Elastoplastic(D,L,a,sm,sp)
            obj.setGeometry(D,L);
            obj.setAV(a);
            obj.setBoundsMSP(diag(1./obj.a)*sm,diag(1./obj.a)*sp)
        end
        
        function setGeometry(obj,D,L)
            %setup#1
            
            %L:R^m ->R^q
            %U=Im D \cap Ker L
            %Ker opV=V=(A U)^perp =A^{-1} (U^perp)
            obj.D=D;
            obj.m=size(D,1);
            obj.n=size(D,2);
            obj.L=L;
            obj.imDOrt=(null(obj.D')).';
            
            %important definition:
            obj.uOrt=[obj.imDOrt; obj.L];
            obj.uBasis=null(obj.uOrt);
            obj.dimU=size(obj.uBasis,2);
            obj.dimV=obj.m-size(obj.uBasis,2);
            %disp(strcat('U dimensions:',num2str(size(obj.uBasis,2))));
            %disp(strcat('V dimensions:',num2str()));
        end
        
        function setAV(obj,a)
            %setup #2
            obj.a=a;
            A=diag(obj.a);
            invA=diag(1./obj.a);
            obj.vOrt=(A*obj.uBasis).';
            
            %setting up piston-vectors in H
            M=[obj.vOrt; obj.imDOrt; obj.L];
            q=size(obj.L,1);
            B=[zeros(size(obj.vOrt,1)+size(obj.imDOrt,1),q); eye(q)];
            obj.pistonsH=M\B;
            
            
            [obj.vBasis, R]=GramSchmidt([obj.pistonsH invA*obj.imDOrt.']); %V=A^{-1} U^\perp, orthonormalized
            
            
            %setting up piston-vectors in V
            
            %pistonsV - coordinates of the piston-vector in V-basis 
            obj.pistonsV=zeros(size(obj.vBasis,2),size(obj.L,1));
            
            %finding the projectors on V and U 
            %transform matrix to uBasis, vBasis coords
            coordsMatr=inv([obj.uBasis obj.vBasis]); %transform matrix to uBasis, vBasis coords
            
            %the projection matrix on U is the upper part of the transform matrix
            obj.uP=coordsMatr(1:size(obj.uBasis,2),:);
            %the projection matrix on V is the lower part of the transform matrix
            obj.vP=coordsMatr(size(obj.uBasis,2)+1:end,:);
            
            obj.pistonsV=obj.vP*obj.pistonsH;
            
            obj.verifyUV(1e-9);
        end
        
        function verifyUV(obj,eps)
            t1=obj.uOrt*obj.uBasis;
            t2=obj.vOrt*obj.vBasis;
            t3=obj.uBasis.'*diag(obj.a)*obj.vBasis;
            t4=obj.vP*obj.vBasis-eye(size(obj.vBasis,2));
            t5=obj.uOrt*(obj.vBasis*obj.vP-eye(obj.m));
            
            if norm(t1)>eps
                error(strcat('Space U failed ~',num2str(norm(t1))));
            end
            if norm(t2)>eps
                error(strcat('Space V failed ~',num2str(norm(t2))));
            end
            if norm(t3)>eps
                error(strcat('U is not A-orthogonal to V ~',num2str(norm(t3))));
            end
            if norm(t4)>eps
                error(strcat('Projection on V failed to be idepotent ~',num2str(norm(t4))));
            end
            if norm(t5)>eps
                error(strcat('Projection on V failed to be orthogonal~',num2str(norm(t5))));
            end
            
        end
        
        function setBoundsMSP(obj,em,ep)
            %setup #3
            obj.em=em;
            obj.ep=ep;
            obj.msp=MSPbp(diag(obj.a),obj.em,obj.ep,obj.vOrt);
        end
        
        function [g,gVcoords]=gByL(obj,l)
            gVcoords=obj.pistonsV*l;
            g=obj.vBasis*gVcoords;
        end
        
        function h=hByCoords(obj,coords)
            h=obj.uBasis*coords;
        end
        
        function f=forceByH(obj,h)
            f=-obj.D.'*diag(obj.a)*h;
        end
        
        function offset=getOffset(obj,l, hCoords)
            if isempty(l)
                offset=obj.hByCoords(hCoords);
            elseif  isempty(hCoords)
                offset=-obj.gByL(l);
            else
                offset=obj.hByCoords(hCoords)-obj.gByL(l);
            end
        end
        
        function [y1, e1]=getElastic(obj,hCoords,l,y,t)
            offset=obj.getOffset(l, hCoords);
            y1=obj.msp.catchup(y, t, offset);
            e1=y1-offset;
        end
        
        function [Neq,Nneq]=getNormalCone(obj,e,tol)
            %error margin            
            
            Neq=[]; %p1-p \in N_C(e) =>  Neq*p1=Neq*p
            Nneq=[]; %p1-p \in N_C(e) =>  Nneq*p1 <=Nneq*p
            %both RHS => p1-p \in N_C(e)
            for i=1:obj.m
                if abs(e(i)-obj.em(i))<tol
                    Nneq=[Nneq; zeros(1,obj.m)];
                    Nneq(end,i)=1;
                elseif abs(e(i)-obj.ep(i))<tol
                    Nneq=[Nneq; zeros(1,obj.m)];
                    Nneq(end,i)=-1;
                else
                    Neq=[Neq; zeros(1,obj.m)];
                    Neq(end,i)=1;
                end
            end
            
        end
        
        function p1=getPlastic(obj,e1,p,l,method,tol)
            %methods: 'interior-point-convex'
            
            [Neq,Nneq]=obj.getNormalCone(e1,tol);
            
            Aeq=[obj.uOrt;Neq];
            
            %geometric constraint part
            if ~isempty(obj.imDOrt)
                beq1=-obj.imDOrt*e1;
            else
                beq1=[];
            end
            
            %additional constraint part
            if ~isempty(obj.L)
                beq2= -obj.L*e1+l;
            else
                beq2=[];
            end
            
            %unsaturated springs part
            if ~isempty(Neq)
                beq3= Neq*p;
            else
                beq3=[];
            end
            
            beq=[beq1;beq2;beq3];
            
            if ~isempty(Nneq)
                %do calculations only if there are saturated springs,
                %otherwise just keep old p
                b=Nneq*p;
                
                %opts = optimoptions('quadprog','Algorithm',,'Display','off');
                opts = optimoptions('quadprog','Algorithm',method,'Display','off');
                
                
                [p1,val,exitflag]=quadprog(eye(obj.m),zeros(1,obj.m),Nneq,b,Aeq,beq,[],[],[],opts);
                if exitflag~=1
                    error(strcat('cannot optimize, fval: ',num2str(val),...
                        ', exitflag: ',num2str(exitflag)))
                end
            else
                p1=p;
                
            end
        end
        
        function verify(obj,t,e,p,prevP,l,hCoords,tol)
            %veryfing balance of forces
            if ~isempty(obj.vOrt)
                quasistaticBalance=obj.vOrt*(e+obj.hByCoords(hCoords));
                if norm(quasistaticBalance,1)>tol;
                    error(strcat('quasistatic balance failed for t=',num2str(t),...
                        '! Error magnitude: ',...
                        num2str(norm(quasistaticBalance,1))));
                end
            end
            
            %veryfing geometric constraint
            if ~isempty(obj.imDOrt)
                geometricConstraint=obj.imDOrt*(e+p);
                if norm(geometricConstraint,1)>tol;
                    error(strcat('geometric constraint failed for t=',num2str(t),...
                        '! Error magnitude: ',...
                        num2str(norm(geometricConstraint,1))));
                end
            end
            
            %verifying pistons constraints
            if ~isempty(obj.L)
                pistonConstraint=obj.L*(e+p)-l;
                if norm(pistonConstraint,1)>tol;
                    error(strcat('piston constraint failed for t=',num2str(t),...
                        '! Error magnitude: ',...
                        num2str(norm(pistonConstraint,1))));
                end
            end
            
            %verifying normal cone
            [Neq,Nneq]=obj.getNormalCone(e,tol);
            
            if ~isempty(Neq)
                pInNeq=Neq*(p-prevP);
                if norm(pInNeq,1)>tol;
                    error(strcat('normal cone equality part failed for t=',num2str(t),...
                        '! Error magnitude: ',...
                        num2str(norm(pInNeq,1))));
                end
            end
            
            if ~isempty(Nneq)
                pInNneq=Nneq*(p-prevP);
                if any(pInNneq>tol)
                    error(strcat('normal cone inequality part failed for t=',num2str(t),...
                        '! All must be nonpositive: ',...
                        num2str(pInNneq)));
                end
            end
        end
        
        function [Y,E]=solveElastic(obj,T,e0,hCoords,l)
            %solve MSP with catchup discretization from array T
            %e0 - initial condition
            %hCoords, l - function handles of t
            
            %y=e+h-g
            %z=e+p+h-g
            
            y=e0+obj.getOffset(l(0), hCoords(0));
            szT=size(T,2);
            %preallocating return arrays
            Y=[y zeros(obj.m,szT-1)];
            E=[e0 zeros(obj.m,szT-1)];
            %doing catchup
            for i=2:szT
                [y,e]=obj.getElastic(hCoords(T(i)),l(T(i)),y,T(i));
                Y(:,i)=y;
                E(:,i)=e;
            end
            
        end
        
        function [P,X]=solvePlastic(obj,T,E,p,l,method,tol)
            %methods:'interior-point-convex'
            
            szT=size(T,2);
            P=[p zeros(obj.m,szT-1)];
            X=[E(:,1)+p zeros(obj.m,szT-1)];
            
            for i=2:szT
                p=obj.getPlastic(E(:,i),p,l(T(i)),method,tol);
                P(:,i)=p;
                X(:,i)=E(:,i)+p;
            end
            
        end
        function verifySolution(obj,T,E,P,l,hCoords,tolerance)
            %test solution
            for i=2:size(T,2)
                obj.verify(T(i),E(:,i),P(:,i),P(:,i-1),l(T(i)),hCoords(T(i)),tolerance);
            end
        end
        
        
        
    end
    
end

