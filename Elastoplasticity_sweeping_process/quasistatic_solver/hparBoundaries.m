%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

function [hmin, hmaxSimplex,hminSimplex,hmax] = hparBoundaries(pr)
%pr - problem
trA=sum(pr.a);
hmin=-sqrt(3)*(pr.a*[pr.ep(1); -pr.em(2);pr.ep(3)])/trA;
hmax=-sqrt(3)*(pr.a*[pr.em(1); -pr.ep(2);pr.em(3)])/trA;

m1=max([pr.a*[pr.em(1); -pr.em(2);pr.ep(3)],...
    pr.a*[pr.ep(1); -pr.ep(2);pr.ep(3)],...
    pr.a*[pr.ep(1); -pr.em(2);pr.em(3)]]);
m2=min([pr.a*[pr.ep(1); -pr.ep(2);pr.em(3)],...
    pr.a*[pr.em(1); -pr.em(2);pr.em(3)],...
    pr.a*[pr.em(1); -pr.ep(2);pr.ep(3)]]);

hmaxSimplex=-sqrt(3)*m1/trA;
hminSimplex=-sqrt(3)*m2/trA;
end

