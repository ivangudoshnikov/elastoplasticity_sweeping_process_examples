%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

%{
EXAMPLE 3

A "simplest nontrivial" configuration of springs
Proposed by prof. D. Rachinskiy

*--------*----------------*  (springs 1,4)
         *-------*           (spring 3)
*----------------*--------*  (spring 2,5)
*=========================*  (additional length constraint)

Parameters: all elastic intervals [-1,1] and
stiffness coefficients=1

Input: fixed constraint length, 
an external force grows linearly(up to max admissible value) in such a
way that the moving set does not become a simplex. 
%}

clear;
init;

%geometry of the springs
D=[-1 1 0 0;...
   -1 0 1 0;...
   0 -1 1 0;
   0 -1 0 1;
   0 0 -1 1];
%external length constraints
R=[1 0 1 0 1];
%stiffness
a=[1 1 1 1 1];
%min and max stress
sm=[-1,-1,-1,-1,-1].';
sp=[1, 1, 1, 1, 1].';
%setting up the problem
problem=Elastoplastic(D,R,a,sm,sp);

%time step and interval
dt=0.005;
tmax=1-dt;
T=0:dt:tmax;


%an alternative basis in U, not normalized, not orthogonal!
uBasisAlt=...
    [-1  0;
      0 -1;
      1 -1;
      1  0;
      0  1];

%external forces as a function of time
hCoords = @(t)t*problem.uP*uBasisAlt*[0;1];
%external constraint length as a function of time
l=@(t)0;

tol=1e-9;
%initial conditions
e0=[0;0;0;0;0];
p0=-e0;

%the solution
[Y,E]=problem.solveElastic(T,e0,hCoords,l);
[P,X]=problem.solvePlastic(T,E,p0,l,'active-set',tol);
problem.verifySolution(T,E,P,l,hCoords,tol);
%figure;
%inpValues=piecewiseLinPer(T,Per,Amp);
%p2=plot(T,inpValues);

%setup a visual appearence of the springs system

%additional constratint on nodes to solve the system for nodes coords
nodeConstr=[1 0 0 0];
%value of the constr
nodeConstrVal=0;
defaultNodesX=[-12; -4; 4; 12];

%y's used in the visualization of the springs
y0=5;
y1=4;
y2=3;
y3=2;
y4=1;

%drawing markers at joints, each has: corresponding node, height 
nodedots=[struct('node',1,'y',y0),...
          struct('node',3,'y',y0),... 
          struct('node',1,'y',y1),... 
          struct('node',2,'y',y1),... 
          struct('node',3,'y',y1),... 
          struct('node',4,'y',y1),...
          struct('node',1,'y',y2),... 
          struct('node',2,'y',y2),...
          struct('node',3,'y',y2),... 
          struct('node',4,'y',y2),... 
          struct('node',2,'y',y3),...
          struct('node',4,'y',y3),...
          struct('node',1,'y',y4),...
          struct('node',4,'y',y4)... 
         ];
%                                    
%                     #=ek          _____
%                  h _/\  /\  _____|   __|__    aw,ah - arrowhead height&width
%                   l1  \/  \/l2 l3    l4 l5

springs=[struct('lnode',1,'rnode',2,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',1,'rnode',3,'defElastLen',8,'y',y0,   'h',0.2,'ek',6, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',2,'rnode',3,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',2,'rnode',4,'defElastLen',8,'y',y3,   'h',0.2,'ek',6, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',3,'rnode',4,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1)...
        ];
    
pistons=[struct('lnode',1,'rnode',4,'y',y4)];     

vertlines=[struct('node',1,'y1',y0,'y2',y4),...
           struct('node',2,'y1',y1,'y2',y3),...
           struct('node',3,'y1',y0,'y2',y2),...
           struct('node',4,'y1',y1,'y2',y4)...
          ];
extForces=struct('y',[y2; y2; y2; y2],'k',1.5,'aw',0.2,'ah',0.04);

springsSetupVisual=SpringsSetupVisual(problem,hCoords,l,T,Y,E,P,X,...
                nodeConstr,nodeConstrVal,defaultNodesX,...
                nodedots,springs,pistons,vertlines,extForces);
showV3D=ShowV3D(problem,hCoords, l,T,Y);
            
axes3Dsize=[-5 5 -5 5 -5 5];
axesSpringsSize=[-15 15 -3 8];
PlayerGUI2axes('Sweeping process in 3D',axes3Dsize,axesSpringsSize,T,showV3D,springsSetupVisual);