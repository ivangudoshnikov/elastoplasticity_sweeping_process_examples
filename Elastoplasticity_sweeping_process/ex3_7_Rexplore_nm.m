%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

%{
EXAMPLE 3

A "simplest nontrivial" configuration of springs
Proposed by prof. D. Rachinskiy

*--------*----------------*  (springs 1,4)
         *-------*           (spring 3)
*----------------*--------*  (spring 2,5)
*=========================*  (additional length constraint)

The EXPLORE tool allows to see how a change of the parameters 
(elastic boundaries and stiffness coefficients) 
influence the shape of the moving set
Here the initial parameter values are such that spring 3 saturates  
and then relaxes 
%}

clear;
init;

%geometry of the springs
D=[-1 1 0 0;...
   -1 0 1 0;...
   0 -1 1 0;
   0 -1 0 1;
   0 0 -1 1];
%external length constraints
R=[1 0 1 0 1];
%stiffness
a=[1 3 2 3 1];
%min and max stress
sp=[1.3, 1.05, 0.3, 1.35, 1.25].';
sm=-sp;
%setting up the problem
problem=ElastoplasticSliding(D,R,a,sm,sp,'interior-point-convex');

%initiate the EXPLORE tool
expl=ExploreV3D([-5 5 -5 5 -5 5],problem,false,false);