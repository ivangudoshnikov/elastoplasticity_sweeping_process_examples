%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

%{
EXAMPLE 1
First example

*--------*--------*  (springs 1 & 2)
*-----------------*  (spring 3)
*=================*  (additional length constraint)

Input: additional length constraint is fixed, an external force grows up
to the max possible value
Initial value: springs 1 & 2 are initially stretched 
%}

clear;
init;

%geometry of the springs: *-*-*
%                         *---*    
D=[-1 1 0; 0 -1 1; -1 0 1];
%external length constraint controls the length of 3rd spring
R=[0 0 1];
%stiffness
a=[1 1 1];
%min and max stress
sm=[-2,-2,-1].';
sp=[2,2,1].';

%setting up the problem
problem=Elastoplastic(D,R,a,sm,sp);

%time step and interval
dt=0.01;
t0=0;
t1=2;
T=t0:dt:t1-dt;

%error tolerance 
tol=1e-13;

%elastic and plastic initial values
e0=[1;1;0];
p0=[-1;-1;0];

%external forces as a function of time
hCoords = @(t)-sqrt(2)*t;
%external constraint length as a function of time
l=@(t)0;

%solving the problem:
[Y,E]=problem.solveElastic(T,e0,hCoords,l);

[P,X]=problem.solvePlastic(T,E,p0,l,'interior-point-convex',tol);
problem.verifySolution(T,E,P,l,hCoords,tol);

%figure;
%p1=plot(T,E);
%legend('show');
%figure;
%p2=plot(T,P);
%legend('show');

%setup a visual appearence of the springs system

%additional constratint on nodes to solve the system for nodes coords
nodeConstr=[1 0 0];
%value of the constr
nodeConstrVal=0;

defaultNodesX=[-8;0;8];

%y's used in the visualization of the springs
y0=4;
y1=3;
y2=2;
y3=1;

%drawing markers at joints, each has: corresponding node, height 
nodedots=[struct('node',1,'y',y0),...
          struct('node',1,'y',y1),...
          struct('node',1,'y',y2),...
          struct('node',1,'y',y3),...
          struct('node',2,'y',y0),...
          struct('node',2,'y',y1),...
          struct('node',3,'y',y0),...
          struct('node',3,'y',y1),...
          struct('node',3,'y',y2),...
          struct('node',3,'y',y3)...
          ];

%                                    
%                     #=ek          _____
%                  h _/\  /\  _____|   __|__    aw,ah - arrowhead height&width
%                   l1  \/  \/l2 l3    l4 l5
springs=[struct('lnode',1,'rnode',2,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',2,'rnode',3,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',1,'rnode',3,'defElastLen',8,'y',y2,   'h',0.2,'ek',6, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
        ];
    
pistons=[struct('lnode',1,'rnode',3,'y',y3)];

vertlines=[struct('node',1,'y1',y0,'y2',y3),...
           struct('node',3,'y1',y0,'y2',y3),...
           struct('node',2,'y1',y0,'y2',y1)...
           ];
%visual representation of the external forces "arrows" drawn at the nodes
%y - y-coords
%k - scale factor
%aw,ah - arrowhead size 
extForces=struct('y',[y0; y0; y0],'k',0.5,'aw',0.2,'ah',0.04);

show3D=Show3D(problem,hCoords, l,T,Y);
springsSetupVisual1=SpringsSetupVisual(problem,hCoords,l,T,Y,E,P,X,...
                nodeConstr,nodeConstrVal,defaultNodesX,...
                nodedots,springs,pistons,vertlines,extForces);

axes3Dsize=[-3 3 -3 3 -3 3];
axesSpringsSize=[-9 9 -1 6];
PlayerGUI2axes('Sweeping process in 3D',axes3Dsize,axesSpringsSize,T,show3D,springsSetupVisual1);
            
            

showV2D=ShowV2D(problem,hCoords, l,T,Y);            
springsSetupVisual2=SpringsSetupVisual(problem,hCoords,l,T,Y,E,P,X,...
                nodeConstr,nodeConstrVal,defaultNodesX,...
                nodedots,springs,pistons,vertlines,extForces);
axesV2Dsize=[-5 5 -5 5];
PlayerGUI2axes('Sweeping process in 3D',axesV2Dsize,axesSpringsSize,T,showV2D,springsSetupVisual2);