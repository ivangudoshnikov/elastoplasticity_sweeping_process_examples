%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}
warning('off','optim:quadprog:WillBeRemoved');
warning('off','MATLAB:timer:deleterunning')
addpath(strcat(pwd,'\quasistatic_solver'));
addpath(strcat(pwd,'\quasistatic_solver\polyhedra'));