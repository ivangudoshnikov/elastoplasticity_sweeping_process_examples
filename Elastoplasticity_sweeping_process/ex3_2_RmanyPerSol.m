%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

%{
EXAMPLE 3

A "simplest nontrivial" configuration of springs
Proposed by prof. D. Rachinskiy

*--------*----------------*  (springs 1,4)
         *-------*           (spring 3)
*----------------*--------*  (spring 2,5)
*=========================*  (additional length constraint)

Parameters: stiffness=1 (all springs), but elastic boundries have 2
different values

Input: no external forces, periodic constraint length

This is a perturbed situaltion from ex3_1_Rbase.m which has 
an entire interval of periodic solutions
%}

clear;
init;

%geometry of the springs
D=[-1 1 0 0;...
   -1 0 1 0;...
   0 -1 1 0;
   0 -1 0 1;
   0 0 -1 1];
%external length constraints
R=[1 0 1 0 1];
%stiffness
a=[1 1 1 1 1];
%min and max stress
sm=[-1,-1,-1,-1.2,-1.2].';
sp=[1, 1, 1, 1.2, 1.2].';
%setting up the problem
problem=Elastoplastic(D,R,a,sm,sp);

%time step and interval
Amp=3; %amplitude
Per=1;%period
NPer=3; %amount of periods

dt=0.01;
T=0:dt:Per*NPer;

tol=1e-9;

%external forces as a function of time
hCoords = @(t)[0;0];
%external constraint length as a function of time
l=@(t)(piecewiseLinPer(t,Per,Amp));

%elastic and plastic initial values
numberOfSolutions=5;

%5 different initial conditions
e0(:,1)=-problem.getOffset(l(0),hCoords(0));
e0(:,2)=-problem.getOffset(l(0),hCoords(0))+problem.vBasis*[0;1;1];
e0(:,3)=-problem.getOffset(l(0),hCoords(0))+problem.vBasis*[0;-1;1];
e0(:,4)=-problem.getOffset(l(0),hCoords(0))+problem.vBasis*[0;-1;-1];
e0(:,5)=-problem.getOffset(l(0),hCoords(0))+problem.vBasis*[0;1;-1];

p0=-e0;

%solving the system
for i=1:numberOfSolutions
    [Y(:,:,i),E(:,:,i)]=problem.solveElastic(T,e0(:,i),hCoords,l);
    [P(:,:,i),X(:,:,i)]=problem.solvePlastic(T,E(:,:,i),p0(:,i),l,'active-set',tol);
    problem.verifySolution(T,E(:,:,i),P(:,:,i),l,hCoords,tol);
end

%setup a visual appearence of the springs system

%additional constratint on nodes to solve the system for nodes coords
nodeConstr=[1 0 0 0];
%value of the constr
nodeConstrVal=0;
defaultNodesX=[-12; -4; 4; 12];

%y's used in the visualization of the springs
y1=4;
y2=3;
y3=2;
y4=1;

%drawing markers at joints, each has: corresponding node, height 
nodedots=[struct('node',1,'y',y1),...
          struct('node',3,'y',y1),... 
          struct('node',1,'y',y2),... 
          struct('node',2,'y',y2),... 
          struct('node',3,'y',y2),... 
          struct('node',4,'y',y2),... 
          struct('node',2,'y',y3),... 
          struct('node',4,'y',y3),...
          struct('node',1,'y',y4),...
          struct('node',4,'y',y4)... 
         ];
     
%                                    
%                     #=ek          _____
%                  h _/\  /\  _____|   __|__    aw,ah - arrowhead height&width
%                   l1  \/  \/l2 l3    l4 l5

springs=[struct('lnode',1,'rnode',2,'defElastLen',4,'y',y2,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',1,'rnode',3,'defElastLen',8,'y',y1,   'h',0.2,'ek',6, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,  'aw',0.1,'ah',0.1),...
         struct('lnode',2,'rnode',3,'defElastLen',4,'y',y2,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',2,'rnode',4,'defElastLen',8,'y',y3,   'h',0.2,'ek',6, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,  'aw',0.1,'ah',0.1),...
         struct('lnode',3,'rnode',4,'defElastLen',4,'y',y2,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1)...
        ];
    
pistons=[struct('lnode',1,'rnode',4,'y',y4)];     

vertlines=[struct('node',1,'y1',y1,'y2',y4),...
           struct('node',2,'y1',y2,'y2',y3),...
           struct('node',3,'y1',y1,'y2',y2),...
           struct('node',4,'y1',y2,'y2',y4)...
          ];
extForces=[];

k=1;%solution to visualize
springsSetupVisual=SpringsSetupVisual(problem,hCoords,l,T,Y(:,:,k),E(:,:,k),P(:,:,k),X(:,:,k),...
                nodeConstr,nodeConstrVal,defaultNodesX,...
                nodedots,springs,pistons,vertlines,extForces);
showV3D=ShowV3D(problem,hCoords, l,T,Y);
            
axes3Dsize=[-5 5 -5 5 -5 5];
axesSpringsSize=[-12 15 -3 8];
PlayerGUI2axes('Sweeping process in 3D',axes3Dsize,axesSpringsSize,T,showV3D,springsSetupVisual);