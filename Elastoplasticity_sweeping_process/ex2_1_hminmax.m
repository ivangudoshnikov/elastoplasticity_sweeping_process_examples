%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}
%{
EXAMPLE 2

*=================*           (additional length constraint 1)
*--------*--------*--------*  (springs 1,2,3)
         *=================*  (additional length constraint 2)
Input: additional length constraints are fixed, the external force goes
through the whole available range to show the change of the moving set 
Initial value: everything is relaxed
%}

clear;
init;
%geometry of the springs: *-*-*-*
D=[-1 1 0 0; 0 -1 1 0; 0 0 -1 1];
%external length constraints
R=[1 1 0; 0 1 1];
%stiffness
a=[1 2 0.9];
%min and max stress
sm=[-1.1,-1.5,-2].';
sp=[1.4,1,0.8].';
%setting up the problem
problem=Elastoplastic(D,R,a,sm,sp);

%hpar(external forces) boundaries
[hminSL, hmaxSimplex,hminSimplex, hmaxSL]= hparBoundaries(problem);


%Simulation setup
dt=0.005;
T=0:dt:1;

%error tolerance 
tol=1e-13;

%external forces as a function of time
hCoords=@(t)piecewiseLinPer(t,1,0.95*(hmaxSL-hminSL)/2)+(hmaxSL+hminSL)/2;
%hCoords = @(t)0.99*((1-t).*hmin+t.*hmax);

%external constraints lengths are constant
l=@(t)[0;0];

%plot external force input - figure 1 
figure('Name','External force input');
plot(T,hCoords(T),[0,1],[hminSL,hminSL],[0,1],[hmaxSL,hmaxSL]);

%elastic and plastic initial values
e0=-problem.getOffset(l(0),hCoords(0));
p0=[0;0;0];

[Y,E]=problem.solveElastic(T,e0,hCoords,l);
[P,X]=problem.solvePlastic(T,E,p0,l,'interior-point-convex',tol);
problem.verifySolution(T,E,P,l,hCoords,tol);

%p1=plot(T,E);
%legend('show');
%figure;
%p2=plot(T,P);
%legend('show');

%setup a visual appearence of the springs system

%additional constratint on nodes to solve the system for nodes coords
nodeConstr=[1 0 0 0];
%value of the constr
nodeConstrVal=0;

defaultNodesX=[-12; -4; 4; 12];

%y-coordinate values used in the visualization of the springs
y0=4;
y1=3;
y2=2;
y3=1;

%drawing markers at joints, each has: corresponding node, height 
nodedots=[struct('node',1,'y',y0),...
          struct('node',1,'y',y1),...
          struct('node',1,'y',y2),...
          struct('node',2,'y',y1),...
          struct('node',2,'y',y2),...
          struct('node',2,'y',y3),...
          struct('node',3,'y',y0),...
          struct('node',3,'y',y1),...
          struct('node',3,'y',y2),...
          struct('node',4,'y',y1),...
          struct('node',4,'y',y2),...
          struct('node',4,'y',y3)...
          ];

%                                    
%                     #=ek          _____
%                  h _/\  /\  _____|   __|__    aw,ah - arrowhead height&width
%                   l1  \/  \/l2 l3    l4 l5

springs=[struct('lnode',1,'rnode',2,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',2,'rnode',3,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',3,'rnode',4,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...         
        ];
    
pistons=[struct('lnode',1,'rnode',3,'y',y0),...
         struct('lnode',2,'rnode',4,'y',y3)...
        ];

vertlines=[struct('node',1,'y1',y0,'y2',y2),...
           struct('node',2,'y1',y1,'y2',y3),...
           struct('node',3,'y1',y0,'y2',y2),...
           struct('node',4,'y1',y1,'y2',y3)
           ];

%visual representation of the external forces "arrows" drawn at the nodes
%y - y-coords
%k - scale factor
%aw,ah - arrowhead size 
extForces=struct('y',[y2; y2; y2; y2],'k',0.5,'aw',0.2,'ah',0.04);

show3D=Show3D(problem,hCoords, l,T,Y);
springsSetupVisual1=SpringsSetupVisual(problem,hCoords,l,T,Y,E,P,X,...
                nodeConstr,nodeConstrVal,defaultNodesX,...
                nodedots,springs,pistons,vertlines,extForces);

axes3Dsize=[-3 3 -3 3 -3 3];
axesSpringsSize=[-12 15 -3 8];
PlayerGUI2axes('Sweeping process in 3D',axes3Dsize,axesSpringsSize,T,show3D,springsSetupVisual1);

showV2D=ShowV2D(problem,hCoords, l,T,Y);            
springsSetupVisual2=SpringsSetupVisual(problem,hCoords,l,T,Y,E,P,X,...
                nodeConstr,nodeConstrVal,defaultNodesX,...
                nodedots,springs,pistons,vertlines,extForces);
axesV2Dsize=[-5 5 -5 5];
PlayerGUI2axes('Sweeping process in 3D',axesV2Dsize,axesSpringsSize,T,showV2D,springsSetupVisual2);