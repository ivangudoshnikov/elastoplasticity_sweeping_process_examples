%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

%{
EXAMPLE 3

A "simplest nontrivial" configuration of springs
Proposed by prof. D. Rachinskiy

*--------*----------------*  (springs 1,4)
         *-------*           (spring 3)
*----------------*--------*  (spring 2,5)
*=========================*  (additional length constraint)

Parameters: stiffness=[1 3 2 3 1], stress boundaries=[1, 1.2, 1, 1, 1]

Input: no external forces, addtional constraint length grows
monotonically.

The example illustrates the following "non-monotonicity" phenomenon:
Despite the additional constraint expanding monotonically,
spring #3 contracts at first and then starts expanding. 
%}

clear;
init;

%geometry of the springs
D=[-1 1 0 0;...
   -1 0 1 0;...
   0 -1 1 0;
   0 -1 0 1;
   0 0 -1 1];
%external length constraints
R=[1 0 1 0 1];
%stiffness
a=[1 3 2 3 1];
%min and max stress
sm=[-1,-1.2,-1,-1,-1].';
sp=[1, 1.2, 1, 1, 1].';
%setting up the problem
problem=Elastoplastic(D,R,a,sm,sp);

%time step and interval
dt=0.02;
t0=0;
t1=3;
T=t0:dt:t1-dt;

%external forces as a function of time
hCoords = @(t)[0;0];
%external constraint length as a function of time
l=@(t)t;

%initial condition
e0=[0;0;0;0;0];
p0=[0;0;0;0;0];

%error tolerance
tol=1e-9;

%solution
[Y,E]=problem.solveElastic(T,e0,hCoords,l);
[P,X]=problem.solvePlastic(T,E,p0,l,'active-set',tol);
problem.verifySolution(T,E,P,l,hCoords,tol);

figure('Name','Elastic elongation');
p1=plot(T,E);
legend('show');

%setup a visual appearence of the springs system

%additional constratint on nodes to solve the system for nodes coords
nodeConstr=[1 0 0 0];
%value of the constr
nodeConstrVal=0;
defaultNodesX=[-3; -1; 1; 3];

%y's used in the visualization of the springs
y1=4;
y2=3;
y3=2;
y4=1;

%drawing markers at joints, each has: corresponding node, height 
nodedots=[struct('node',1,'y',y1),...
          struct('node',3,'y',y1),... 
          struct('node',1,'y',y2),... 
          struct('node',2,'y',y2),... 
          struct('node',3,'y',y2),... 
          struct('node',4,'y',y2),... 
          struct('node',2,'y',y3),...
          struct('node',4,'y',y3),...
          struct('node',1,'y',y4),...
          struct('node',4,'y',y4)... 
         ];
     
%                                    
%                     #=ek          _____
%                  h _/\  /\  _____|   __|__    aw,ah - arrowhead height&width
%                   l1  \/  \/l2 l3    l4 l5

springs=[struct('lnode',1,'rnode',2,'defElastLen',1,'y',y2,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.2,'l5',0.2,'aw',0.1,'ah',0.1),...
         struct('lnode',1,'rnode',3,'defElastLen',2,'y',y1,   'h',0.2,'ek',6, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.2,'l5',0.2,'aw',0.1,'ah',0.1),...
         struct('lnode',2,'rnode',3,'defElastLen',1,'y',y2,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.2,'l5',0.2,'aw',0.1,'ah',0.1),...
         struct('lnode',2,'rnode',4,'defElastLen',2,'y',y3,   'h',0.2,'ek',6, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.2,'l5',0.2,'aw',0.1,'ah',0.1),...
         struct('lnode',3,'rnode',4,'defElastLen',1,'y',y2,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.2,'l5',0.2,'aw',0.1,'ah',0.1)...
        ];
      
pistons=[struct('lnode',1,'rnode',4,'y',y4)];     

vertlines=[struct('node',1,'y1',y1,'y2',y4),...
           struct('node',2,'y1',y2,'y2',y3),...
           struct('node',3,'y1',y1,'y2',y2),...
           struct('node',4,'y1',y2,'y2',y4)...
          ];

extForces=[];


springsSetupVisual=SpringsSetupVisual(problem,hCoords,l,T,Y,E,P,X,...
                nodeConstr,nodeConstrVal,defaultNodesX,...
                nodedots,springs,pistons,vertlines,extForces);
showV3D=ShowV3D(problem,hCoords, l,T,Y);
            
axes3Dsize=[-5 5 -5 5 -5 5];
axesSpringsSize=[-3 6 -3 8];
PlayerGUI2axes('Sweeping process in 3D',axes3Dsize,axesSpringsSize,T,showV3D,springsSetupVisual);