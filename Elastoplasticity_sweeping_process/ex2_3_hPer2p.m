%{

Elatoplastic springs and sweeping process by Ivan Gudoshnikov, 2018
ivangudoshnikov@gmail.com

%}

%{
EXAMPLE 2
Illustrates convergence to a single periodic solution when a periodic
moving set is a simplex.


*=================*           (additional length constraint 1)
*--------*--------*--------*  (springs 1,2,3)
         *=================*  (additional length constraint 2)
Input: periodic, but the external force remains in the interval where the
moving set is a triangle
%}

clear;
init;
%geometry of the springs: *-*-*-*
D=[-1 1 0 0; 0 -1 1 0; 0 0 -1 1];
%external length constraints
R=[1 1 0; 0 1 1];
%stiffness
a=[1 1.2 0.9];
%min and max stress
sm=[-1.1,-1.5,-2].';
sp=[1.4,1,0.8].';
%setting up the problem
problem=Elastoplastic(D,R,a,sm,sp);

%hpar boundaries
[hminSL, hmaxSimplex,hminSimplex, hmaxSL]= hparBoundaries(problem);

%time step and interval
dt=0.005;
Per=1;%period
NPer=5; %amount of periods
T=0:dt:Per*NPer;
framePer=round(Per/dt);

%error tolerance 
tol=1e-13;

%bounds of h(external foces) for this example:
hmin=0.4*hminSL+0.6*hmaxSimplex;
hmax=hmaxSimplex;

%external forces as a function of time
hCoords=@(t)piecewiseLinPer(t,Per,(hmax-hmin)/2)+(hmax+hmin)/2;

%external constraint length as a function of time
l=@(t)[piecewiseLinPer(t,Per,1.2);piecewiseLinPer(t,Per/2,1.1)];

%plot external force input - figure 1 
figure('Name','External force input');
plot(T,hCoords(T),[0,T(end)],[hminSL,hminSL],[0,T(end)],[hmaxSL,hmaxSL],[0,T(end)],[hmaxSimplex,hmaxSimplex],[0,T(end)],[hminSimplex,hminSimplex]);
legend('magnitude(hpar)','min valid mag','max valid mag','max simplex mag','min simplex mag');

%elastic and plastic initial values
numberOfSolutions=2;

e0(:,1)=-problem.getOffset(l(0),hCoords(0));
e0(:,2)=-problem.getOffset(l(0),hCoords(0))-problem.gByL([-0.7;0.3]);

p0=-e0;

%solving the system
for i=1:numberOfSolutions
    [Y(:,:,i),E(:,:,i)]=problem.solveElastic(T,e0(:,i),hCoords,l);
    [P(:,:,i),X(:,:,i)]=problem.solvePlastic(T,E(:,:,i),p0(:,i),l,'interior-point-convex',tol);
    problem.verifySolution(T,E(:,:,i),P(:,:,i),l,hCoords,tol);
end

%figure 2
periodDifferencePlot(T,E(:,:,1),framePer,framePer+1)

%setup a visual appearence of the springs system

%additional constratint on nodes to solve the system for nodes coords
nodeConstr=[1 0 0 0];
%value of the constr
nodeConstrVal=0;

defaultNodesX=[-12; -4; 4; 12];

%y-coordinate values used in the visualization of the springs
y0=4;
y1=3;
y2=2;
y3=1;

%drawing markers at joints, each has: corresponding node, height 
nodedots=[struct('node',1,'y',y0),...
          struct('node',1,'y',y1),...
          struct('node',1,'y',y2),...
          struct('node',2,'y',y1),...
          struct('node',2,'y',y2),...
          struct('node',2,'y',y3),...
          struct('node',3,'y',y0),...
          struct('node',3,'y',y1),...
          struct('node',3,'y',y2),...
          struct('node',4,'y',y1),...
          struct('node',4,'y',y2),...
          struct('node',4,'y',y3)...
          ];

%                                    
%                     #=ek          _____
%                  h _/\  /\  _____|   __|__    aw,ah - arrowhead height&width
%                   l1  \/  \/l2 l3    l4 l5

springs=[struct('lnode',1,'rnode',2,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',2,'rnode',3,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...
         struct('lnode',3,'rnode',4,'defElastLen',4,'y',y1,   'h',0.2,'ek',3, 'l1',0.4,'l2',0.1,'l3',0.2,'l4',0.5,'l5',0.5,'aw',0.1,'ah',0.1),...         
        ];
    
pistons=[struct('lnode',1,'rnode',3,'y',y0),...
         struct('lnode',2,'rnode',4,'y',y3)...
        ];

vertlines=[struct('node',1,'y1',y0,'y2',y2),...
           struct('node',2,'y1',y1,'y2',y3),...
           struct('node',3,'y1',y0,'y2',y2),...
           struct('node',4,'y1',y1,'y2',y3)
           ];

%visual representation of the external forces "arrows" drawn at the nodes
%y - y-coords
%k - scale factor
%aw,ah - arrowhead size 
extForces=struct('y',[y2; y2; y2; y2],'k',0.5,'aw',0.2,'ah',0.04);

k=1;%solution to visualize

show3D=Show3D(problem,hCoords, l,T,Y);
springsSetupVisual1=SpringsSetupVisual(problem,hCoords,l,T,Y(:,:,k),E(:,:,k),P(:,:,k),X(:,:,k),...
                nodeConstr,nodeConstrVal,defaultNodesX,...
                nodedots,springs,pistons,vertlines,extForces);

axes3Dsize=[-3 3 -3 3 -3 3];
axesSpringsSize=[-12 15 -3 8];
PlayerGUI2axes('Sweeping process in 3D',axes3Dsize,axesSpringsSize,T,show3D,springsSetupVisual1);

showV2D=ShowV2D(problem,hCoords, l,T,Y);            
springsSetupVisual2=SpringsSetupVisual(problem,hCoords,l,T,Y(:,:,k),E(:,:,k),P(:,:,k),X(:,:,k),...
                nodeConstr,nodeConstrVal,defaultNodesX,...
                nodedots,springs,pistons,vertlines,extForces);
axesV2Dsize=[-5 5 -5 5];
PlayerGUI2axes('Sweeping process in 2D',axesV2Dsize,axesSpringsSize,T,showV2D,springsSetupVisual2);

%plot the trajectories as dotted, but only the last period as solid -
%figure 6
figure('Name','Elastic components');
plot3(E(1,1:end-framePer,1),E(2,1:end-framePer,1),E(3,1:end-framePer,1),':r');
hold on
plot3(E(1,1:end-framePer,2),E(2,1:end-framePer,2),E(3,1:end-framePer,2),':b');

plot3(E(1,end-framePer:end,1),E(2,end-framePer:end,1),E(3,end-framePer:end,1),'r');
plot3(E(1,end-framePer:end,2),E(2,end-framePer:end,2),E(3,end-framePer:end,2),'b');
hold off

